#!/usr/bin/env bash
set -x #bash verbose

##############################################################
#
#	Deluge Daemon
#	Ubuntu
#
##############################################################

#deluge_home='/home/vpn-user'

# Install PPA
apt update -qq
apt install software-properties-common -qqy
add-apt-repository ppa:deluge-team/stable -y

# Add User
#useradd -s /bin/false -d $deluge_home -c "VPN User" -g media -u 2006 -r vpn-user

# Create Directory
mkdir -p $deluge_home/Deluge
mkdir -p /var/log/deluge
chowner -R vpn-user:media $deluge_home/Deluge /var/log/deluge
chmod -R 750 $deluge_home/Deluge /var/log/deluge
mkdir -p $deluge_home/.config
ln -s $deluge_home/Deluge $deluge_home/.config/deluge

# Permissions
wget -qO $deluge_home/Deluge/deluge.permissions.sh https://gitlab.com/0DayByte/servers/raw/master/Networking/Deluge/config/deluge.permissions.sh
wget -qO /etc/systemd/system/deluged.service https://gitlab.com/0DayByte/servers/raw/master/Networking/Deluge/config/deluged.service
wget -qO /etc/systemd/system/deluge-web.service https://gitlab.com/0DayByte/servers/raw/master/Networking/Deluge/config/deluge-web.service
wget -qO /etc/systemd/system/socat-deluged.service https://gitlab.com/0DayByte/servers/raw/master/Networking/Deluge/config/socat-deluged.service
wget -qO /etc/systemd/system/socat-deluge-web.service https://gitlab.com/0DayByte/servers/raw/master/Networking/Deluge/config/socat-deluge-web.service


# Install
apt-get -qqy install deluged deluge-web deluge-console

# Set Permissions
. $deluge_home/Deluge/deluge.permissions.sh

# Start and Enable Service
systemctl start deluged socat-deluged
systemctl enable deluged socat-deluged
systemctl enable deluge-web socat-deluge-web
systemctl start deluge-web socat-deluge-web

#EOF