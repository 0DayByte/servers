#!/usr/bin/env bash
set -x #bash verbose
echo "Starting with Setting Deluge Permissions"

##################################################
#
#	Permissions for Deluged
#
#
####################################################

# Set Permissions for Nginx Configs
chown -R vpn-user:media /home/vpn-user/Deluge /var/log/deluge
find /home/vpn-user/Deluge/ -type f -print0 | xargs -0 chmod 2664
find /home/vpn-user/Deluge/ -type d -print0 | xargs -0 chmod 2775
find /var/log/deluge -type f -print0 | xargs -0 chmod 2660
find /var/log/deluge -type d -print0 | xargs -0 chmod 2770
chown -R root:root /home/vpn-user/Deluge/deluge.permissions.sh
chmod 2600 /home/vpn-user/Deluge/deluge.permissions.sh
chmod +x /home/vpn-user/Deluge/deluge.permissions.sh

echo "Finished with Setting Deluge Permissions"

#EOF