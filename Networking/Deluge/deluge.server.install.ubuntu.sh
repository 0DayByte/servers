#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		Deluge Server Install with PIA on Ubuntu
#
###############################################################
###############################################################
#
#   VARIABLES
#hostname='deluge'
#domain='example.com'
#packages='deb'
#distro='ubuntu'
#distro_version='18.04'
#code_name='bionic'
#interface='eth0'
#fail2ban_ignore_ip='123.123.123.113'
#samba_workgroup='WORKGROUP'
#
###############################################################

echo "Starting with installing deluge secure server"

# Add Media Group
groupadd -g 2000 media

# System Configs
mkdir system
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.conf.sh
. sysctl.conf.sh

# Iptables
mkdir -p $DIR/system
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/IPTables/iptables.install.sh
. iptables.install.sh
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/Deluge/config/iptables/rules.v4 -O /etc/iptables/rules.v4
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/Deluge/config/iptables/rules.v6 -O /etc/iptables/rules.v6
iptables-restore < /etc/iptables/rules.v4
ip6tables-restore < /etc/iptables/rules.v6

# Fail2Ban
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/Fail2Ban/fail2ban.install.sh
. fail2ban.install.sh

# SSL
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/openssl.install.sh
. openssl.install.sh

# Nginx
mkdir -p $DIR/webserver
cd $DIR/webserver
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/nginx.install.sh
. nginx.install.sh

# Postfix
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/postfix.install.sh
. postfix.install.sh

# Samba
mkdir -p $DIR/fileserver
cd $DIR/fileserver
wget https://gitlab.com/0DayByte/servers/raw/master/FileServer/Samba/samba.intall.deb.sh
. samba.intall.deb.sh

# VSFTPD
cd $DIR/fileserver
wget https://gitlab.com/0DayByte/servers/raw/master/FileServer/Vsftp/vsftpd.install.deb.sh
. vsftpd.install.deb.sh

# File Browser
cd $DIR/fileserver
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/FileBrowser/filebrowser.install.deb.sh
. filebrowser.install.deb.sh

# Deluge
cd $DIR/fileserver
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/Deluge/deluged.install.ubuntu.sh
. deluged.install.ubuntu.sh

# OpenVPN
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/OpenVPN/openvpn.client.install.deb.sh
. openvpn.client.install.deb.sh

# OpenSSH
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/openssh.install.sh
. openssh.install.sh

#EOF