#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#                        DNSMasq
#                    Ubuntu/Debian 
#
###############################################################

# Install
apt-get -qq update
apt-get -qqy install dnsmasq

# Remove Configs
rm /etc/dnsmasq.conf

# Set Configs
mkdir -p /etc/dnsmasq.hosts
wget -O /etc/dnsmasq.conf https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSMasq/config/dnsmasq.conf
wget -O /etc/dnsmasq.d/config https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSMasq/config/dnsmasq.d/main.conf
wget -O /etc/dnsmasq.d/dhcp https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSMasq/config/dnsmasq.d/dhcp.conf
wget -O /etc/dnsmasq.d/dns.servers https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSMasq/config/dnsmasq.d/dns.conf
wget -O /etc/dnsmasq.d/forwarding https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSMasq/config/dnsmasq.d/forwarding.conf
wget -O /etc/dnsmasq.d/mail.ldap https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSMasq/config/dnsmasq.d/mail.ldap.conf
wget -O /etc/dnsmasq.hosts/hosts.addons https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSMasq/config/dnsmasq.d/hosts.addons

###############################################################
#
# DNS SERVERS
#
###############################################################

rm /etc/resolv.conf
cat >/etc/resolv.conf <<EOL
search ${domain}
nameserver 127.0.0.1
EOL

cat /etc/resolv.conf

# Restart
systemctl restart dnsmasq

#EOF