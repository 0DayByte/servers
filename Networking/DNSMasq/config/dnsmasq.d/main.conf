# Port Number
port=53

# If you want dnsmasq to listen for DHCP and DNS requests only on
# specified interfaces (and the loopback) give the name of the
# interface (eg eth0) here.
# Repeat the line for more than one interface.
#interface=enp9s0
# Or you can specify which interface _not_ to listen on
#except-interface=
except-interface=tun0

# Pass through DNSSEC validation results from dnscrypt-proxy.
proxy-dnssec

# Reject (and log) addresses from upstream nameservers which are in
# the private IP ranges. This blocks an attack where a browser behind
# a firewall is used to probe machines on the local network.
stop-dns-rebind

# Exempt 127.0.0.0/8 from rebinding checks. This address range is
# returned by realtime black hole servers, so blocking it may disable
# these services. Or stop rebinding on a domain.
#rebind-domain-ok=corp-invest.net
rebind-localhost-ok


# If you don't want dnsmasq to read /etc/resolv.conf or any other
# file, getting its servers from this file instead (see below), then
# uncomment this.
no-resolv

# If you don't want dnsmasq to poll /etc/resolv.conf or other resolv
# files for changes and re-read them then uncomment this.
no-poll

# If you don't want dnsmasq to read /etc/hosts, uncomment the
# following line.
no-hosts

# Never forward plain names (without a dot or domain part)
domain-needed
# Never forward addresses in the non-routed address spaces.
bogus-priv

# Set the cachesize here.
cache-size=10000

# Normally responses which come from /etc/hosts and the DHCP lease
# file have Time-To-Live set as zero, which conventionally means
# do not cache further. If you are happy to trade lower load on the
# server for potentially stale date, you can set a time-to-live (in
# seconds) here.
local-ttl=345600

# If you want dnsmasq to detect attempts by Verisign to send queries
# to unregistered .com and .net hosts to its sitefinder service and
# have dnsmasq instead return the correct NXDOMAIN response, uncomment
# this line. You can add similar lines to do the same for other
# registries which have implemented wildcard A records.
bogus-nxdomain=64.94.110.11

# For debugging purposes, log each DNS query as it passes through
# dnsmasq.
#log-queries