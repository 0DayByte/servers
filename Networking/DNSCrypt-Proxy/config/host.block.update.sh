##!/usr/bin/env bash
set -x #bash verbose

##########################################
#
#	Weekly Crobjob Script
#	for Pi-Hole/NoTracking
#	with DnsCrypt proxy block list
#	and local blocklist
#
#
# - cronjob 
#   5 5 20 * * /root/.scripts/host.block.update.sh
##########################################

DNSCRYPT='/opt/dnscrypt-proxy'


###################### Upgrade DNSCrypt Blocklist

cd ${DNSCRYPT}/block-list

# Remove Old
rm dnscrypt-blacklist-domains.txt
rm ${DNSCRYPT}/app/dnscrypt-blacklist-domains.txt

# Run Pyton Script
python generate-domains-blacklist.py > dnscrypt-blacklist-domains.txt

# Copy
cp dnscrypt-blacklist-domains.txt ${DNSCRYPT}/app/

# Restart
systemctl restart dnscrypt-proxy


##################### Update DNSMasq PI-Hole 

# Remove Old Pi-Hole/NoTracking Host Files
rm -f /etc/dnsmasq.hosts/block.hosts
rm -f /etc/dnsmasq.d/notracking.redirect

# Download Pi-Hole/NoTracking Host Files
wget -qO /etc/dnsmasq.d/notracking.redirect.temp  https://raw.githubusercontent.com/notracking/hosts-blocklists/master/domains.txt
wget -qO /etc/dnsmasq.hosts/block.hosts.temp  https://raw.githubusercontent.com/notracking/hosts-blocklists/master/hostnames.txt
# Remove IP6 for PI-Hole
sed -n '/::/!p' /etc/dnsmasq.d/notracking.redirect.temp > /etc/dnsmasq.hosts/notracking.hosts
sed -n '/::/!p' /etc/dnsmasq.hosts/block.hosts.temp > /etc/dnsmasq.hosts/block.hosts
#sed -n '/^\s*$/!p' /opt/DNSCrypt-Blocklist/block.pre.hosts > /opt/DNSCrypt-Blocklist/block.hosts
#sed -i -e 's/^/0.0.0.0 /' /opt/DNSCrypt-Blocklist/block.hosts
#mv /opt/DNSCrypt-Blocklist/block.hosts /etc/dnsmasq.hosts/


# Remove Temp Files
rm -f /etc/dnsmasq.hosts/block.hosts.temp
rm -f /etc/dnsmasq.d/notracking.redirect.temp

# Restart DNSMasq
systemctl restart dnsmasq