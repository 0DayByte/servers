##!/usr/bin/env bash
set -x #bash verbose

##########################################
#
#   DNSCrypt V2
#
##########################################

VERSION=2.0.7
INSTALL_DIR='/opt/dnscrypt-proxy'

# Reguirements
apt -q update
apt -qqy install git

# Make Directories / Download
mkdir -p ${INSTALL_DIR}
cd /tmp
wget https://github.com/jedisct1/dnscrypt-proxy/releases/download/${VERSION}/dnscrypt-proxy-linux_x86_64-${VERSION}.tar.gz
tar -xvzf dnscrypt-proxy-linux_x86_64-${VERSION}.tar.gz
mv linux-x86_64 ${INSTALL_DIR}/app
git clone https://github.com/jedisct1/dnscrypt-proxy.git /tmp/dnscrypt
mv /tmp/dnscrypt/utils/generate-domains-blacklists ${INSTALL_DIR}/block-list

# Configs
wget -qO /etc/systemd/system/dnscrypt-proxy.service https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSCrypt-Proxy/config/dnscrypt-proxy.service
wget -qO /etc/systemd/system/dnscrypt-proxy.socket https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSCrypt-Proxy/config/dnscrypt-proxy.socket
wget -qO ${INSTALL_DIR}/app/dnscrypt-proxy.toml https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSCrypt-Proxy/config/dnscrypt-proxy.toml
mkdir -p /root/.scripts
wget -qO /root/.scripts/host.block.update.sh https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSCrypt-Proxy/config/host.block.update.sh 
(crontab -l 2>/dev/null; echo "5 5 20 * * /root/.scripts/host.block.update.sh") | crontab -
chmod +x /root/.scripts/host.block.update.sh
cd ${INSTALL_DIR}/block-list/
rm domains-whitelist.txt
rm domains-blacklist.conf
touch domains-whitelist.txt
wget -qO ${INSTALL_DIR}/block-list/domains-blacklist.conf https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSCrypt-Proxy/config/domains-blacklist.conf
python generate-domains-blacklist.py > dnscrypt-blacklist-domains.txt
cp dnscrypt-blacklist-domains.txt ${INSTALL_DIR}/app/

# Systemd Enable/Start
systemctl enable dnscrypt-proxy
systemctl start dnscrypt-proxy