#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
# 		  Firewalld to IPTables
#    		  without IPV6
# 		     Red Hat/CentOS
#
##############################################

# Stop and Remove Firewalld
systemctl stop firewalld
systemctl mask firewalld
yum -q -y remove firewalld

# Install IPTables
yum -q -y install iptables-services

# Setup IPTables
rm -f /etc/sysconfig/{iptables,ip6tables}
wget -O /etc/sysconfig/iptables ${iptables_v4}
sh -c 'iptables-restore -t < /etc/sysconfig/iptables'

# Start IPTables / Disable IPV6
systemctl stop ip6tables
systemctl mask ip6tables
systemctl enable iptables
systemctl start iptables

#EOF