#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#   Firewalld to IPTables with IPV6
#   Debian/Ubuntu
#
##############################################

# Install IPTABLES PERSIT
apt-get -qqy install iptables-persistent

# Set Permissions
chown -R root:root /etc/iptables
chmod 2700 /etc/iptables
chmod 2600 /etc/iptables/*

#EOF
