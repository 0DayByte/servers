#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#   IPTables Config with IPV6
#   Debian/Ubuntu
#
##############################################

# Backup Open IPTables
mv /etc/iptables/rules.v4 /etc/iptables/open.rules.v4
mv /etc/iptables/rules.v6 /etc/iptables/open.rules.v6

# Set IPTables
wget -O /etc/iptables/rules.v4 ${iptables_v4}
wget -O /etc/iptables/rules.v6 ${iptables_v6}

# Set Permissions
chown -R root:root /etc/iptables
chmod 2700 /etc/iptables
chmod 2600 /etc/iptables/*

#EOF
