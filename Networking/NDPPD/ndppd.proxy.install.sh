IPv6SUBNET="xxxx:xxxx:xxxx:xxxx::/64"

echo "######################## CONFIG SYSCTL #########################"
echo "net.ipv6.conf.all.accept_ra = 2" >> /etc/sysctl.conf
echo "net.ipv6.conf.eth0.accept_ra = 2" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.forwarding=1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.proxy_ndp=1" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.forwarding=1" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.proxy_ndp=1" >> /etc/sysctl.conf
echo "net.ipv6.conf.eth0.proxy_ndp=1" >> /etc/sysctl.conf
echo "net.ipv6.ip_nonlocal_bind = 1" >> /etc/sysctl.conf
sysctl -p

echo "############################# ROUTE ############################"
ip -6 route add local $IPv6SUBNET dev lo

echo "######################### INSTALL NDPPD ########################"
# install requirements
apt install ndppd -y


# config
cat <<EOF > /etc/ndppd.conf
route-ttl 30000
address-ttl 30000
proxy eth0 {
router yes
timeout 500
autowire no
keepalive yes
retries 3
promiscuous no
ttl 30000
rule $IPv6SUBNET {
    static
    autovia no
    }
}

systemctl restart ndppd