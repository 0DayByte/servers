#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
# Synthing Install
# Debian/Ubuntu
#
# ref. https://apt.syncthing.net/
#########################################################

# Install requirements
apt-get -qqy install apt-transport-https

# Add Repo
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list

# Install
apt-get -qq update
apt-get -qqy install syncthing

# Enable
systemctl enable syncthing@root
systemctl start syncthing@root

#EOF