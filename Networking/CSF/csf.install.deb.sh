#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#       CSF Firewall
#
###############################################################

# Requited
apt update -q
apt-get -qqy install libwww-perl libio-socket-ssl-perl libcrypt-ssleay-perl libnet-libidn-perl libio-socket-inet6-perl libsocket6-perl ipset

# Download
mkdir -p /usr/src/install
mkdir -p /usr/src/install/network
cd /usr/src/install/network
wget http://download.configserver.com/csf.tgz
tar -xvzf csf.tgz
cd csf   

# Remove Configs
bash install.sh
perl /usr/local/csf/bin/csftest.pl

# Config
wget -O /etc/csf/csf.conf https://gitlab.com/0DayByte/servers/raw/master/Networking/CSF/config/csf.conf

# Start CSF
csf -s

# Reload CSF
csf -r

#EOF
