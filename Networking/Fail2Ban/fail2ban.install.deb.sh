#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#                       Fail2Ban
#                    Ubuntu/Debian 
#
###############################################################

#hostname='hostname'
#domain='example.com'
# Seperate multiple IPs with space
#fail2ban_ignore_ip='123.123.123.123 234.234.234.234'

# Install Fail2Ban
apt-get install fail2ban -qqy
systemctl stop fail2ban

# Configs
wget -qO /etc/fail2ban/jail.conf https://gitlab.com/0DayByte/servers/raw/master/Networking/Fail2Ban/configs/jail.conf
sed -i "s/1.1.1.1/${fail2ban_ignore_ip}/" /etc/fail2ban/jail.conf
sed -i "s/example.com/${hostname}.${domain}/g" /etc/fail2ban/jail.conf
systemctl start fail2ban

#EOF
