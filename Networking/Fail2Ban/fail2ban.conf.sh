#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#                   Fail2Ban Configs
#
###############################################################

# Romove Configs
rm /etc/fail2ban/jail.conf

# Set Configs
wget -O /etc/fail2ban/jail.conf ${fail2ban}

# Restart
systemctl restart fail2ban

#EOF