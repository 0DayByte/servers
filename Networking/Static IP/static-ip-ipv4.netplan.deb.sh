#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
# STATIC NETWORK
# sets repos for packes
# in sources.list in apt
#
###############################################################
###############################################################
#
#   VARIBLES
#interface='eth0'
#domain='exampl.com'
#ip4_address='192.168.1.5/24'
#ip4_gateway='192.168.1.1'
#ip4_dns1='192.168.1.1'
#netplan_file='50-cloud-init'
#
##############################################################

# Remove 
rm /etc/netplan/${netplan_file}.yaml

# Set Repos
cat >/etc/netplan/${netplan_file}.yaml <<EOL
# This file is generated from information provided by
# the datasource.  Changes to it will not persist across an instance.
# To disable cloud-init's network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
network:
  version: 2
  renderer: networkd
  ethernets:
    {interface}:
      dhcp4: no
      addresses:
        - ${ip4_address}
      gateway4: ${ip4_gateway}
      nameservers:
          search: [ ${domain} ]
          addresses:
            - ${ip4_dns1}

# Apply Network Configs
netplan apply

#EOF
