#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
# STATIC NETWORK
# sets repos for packes
# in sources.list in apt
#
###############################################################
###############################################################
#
#   VARIBLES
#interface='eth0'
#domain='example.com'
#ip4_address='192.168.1.5/24'
#ip4_gateway='192.168.1.1'
#ip6_address='2345:6n90:40::4/48'
#ip6_gateway='2345:6n90:40::1'
#ip4_dns1='192.168.1.1'
#ip4_dns2='1.1.1.1'
#ip6_dns1=2345:6n90:40::1'
#ip6_dns2='2606:4700:4700::1111'
#netplan_file='50-cloud-init'
#
##############################################################

# Remove 
rm /etc/netplan/${netplan_file}.yaml

# Set Repos
cat >/etc/netplan/${netplan_file}.yaml <<EOL
# This file is generated from information provided by
# the datasource.  Changes to it will not persist across an instance.
# To disable cloud-init's network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
network:
  version: 2
  renderer: networkd
  ethernets:
    {interface}:
      dhcp4: no
      dhcp6: no
      addresses:
        - ${ip4_address}
        - ${ip6_address}
      gateway4: ${ip4_gateway}
      gateway6: ${ip6_gateway}
      nameservers:
          search: [ ${domain} ]
          addresses:
            - ${ip4_dns1}
            - ${ip4_dns2}
            - ${ip6_dns1}
            - ${ip6_dns2}
EOL

# Apply Network Configs
netplan apply

#EOF