#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
# Install of OpenVPN client
# on Headless server
#
# https://www.privateinternetaccess.com/forum/discussion/3093/raspberry-pi-issues-solved
########################################################

#code_name='bionic'

# Add Repo
wget -qO - https://swupdate.openvpn.net/repos/repo-public.gpg|apt-key add -
echo "deb http://build.openvpn.net/debian/openvpn/stable $code_name main" > /etc/apt/sources.list.d/openvpn-aptrepo.list
apt-get update -q

# Install
apt-get -qqy install network-manager-openvpn openvpn unzip

# Setup PIA
cd /etc/openvpn
wget -q https://www.privateinternetaccess.com/openvpn/openvpn.zip
unzip openvpn.zip

# Delete all ovpn that will not be used

# Create login.conf at root of /etc/openvpn 
# and add login to vpn eg
# login.conf > file
# username
# pass

# Change all .ovpn to .conf

# Uncomment AUTOSTART with confs file name East.conf e.g. AUTOSTART='East'
# in /etc/default/openvpn

#EOF