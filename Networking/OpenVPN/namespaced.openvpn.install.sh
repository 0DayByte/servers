#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
# Install of OpenVPN Name Space
# on Headless server
#
# https://github.com/rakshasa/rtorrent/wiki/VPN-with-Traffic-Splitting
# 
########################################################

# Install Requirements
apt-get install openvpn iproute2 python sudo dnsutils curl -qqy


# Install OpenVPN Name Space
cd /usr/local/sbin
curl -sLSO https://raw.githubusercontent.com/slingamn/namespaced-openvpn/master/namespaced-openvpn
chown root:root namespaced-openvpn
chmod 755 namespaced-openvpn

# Create Systemd File
wget -qO /lib/systemd/system/namespaced-openvpn@.service https://gitlab.com/0DayByte/servers/raw/master/Networking/OpenVPN/config/namespaced-openvpn.service


#EOF