#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		DropBear Install
#       Debian / Uubnut
#       run after SSH install
#
###############################################################

# DropBear
sed -i 's/BUSYBOX=auto/BUSYBOX=y/' /etc/initramfs-tools/initramfs.conf
apt install busybox dropbear dropbear-initramfs -qqy

# Copy and Run Below
ln -s /usr/lib/dropbear/dropbearconvert /usr/bin
sed -i 's/#DROPBEAR_OPTIONS=/DROPBEAR_OPTIONS="-p 55555 -s -j -k -I 180"/' /etc/dropbear-initramfs/config
wget -qO /etc/dropbear-initramfs/authorized_keys https://gitlab.com/0DayByte/public-keys/raw/master/RSA/Erik/erik.dropbear.id.rsa.pub 
dropbearconvert openssh dropbear /etc/ssh/ssh_host_rsa_key /etc/dropbear/dropbear_rsa_host_key
update-initramfs -u
grub-mkconfig -o /boot/grub/grub.cfgcd
update-rc.d dropbear disable

# EOF