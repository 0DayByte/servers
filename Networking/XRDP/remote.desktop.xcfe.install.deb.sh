#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#           Remote Desktop
#           XRDP XFCE 
#
###############################################################

user='username'

# Install Requirements
apt-get update -q
apt-get dist-upgrade -q
apt-get install --install-recommends xfce4 xfce4-goodies xorg dbus-x11 x11-xserver-utils xrdp xorgxrdp sudo -qqy

# Setup XRDP
systemctl stop xrdp
adduser xrdp ssl-cert
exec startxfce4
echo "exec startxfce4" >> /etc/xrdp/xrdp.ini
sleep 5
systemctl start xrdp

# Create User
useradd -d /home/${user} -m ${user}
touch /etc/sudoers.d/${user}
echo "${user} ALL=(ALL:ALL) NOPASSWD: ALL" /etc/sudoers.d/${user}
sudo passwd ${user}

# Install Chrome
cd /tmp
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt-get install ./wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -qqy

# Reboot
reboot

#EOF