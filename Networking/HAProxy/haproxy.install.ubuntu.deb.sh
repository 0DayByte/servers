#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		HAProxy Install
#       Ubuntu 
#
###############################################################

# Add Repo
apt install curl -y
curl https://haproxy.debian.net/bernat.debian.org.gpg | apt-key add -
apt install software-properties-common
add-apt-repository ppa:vbernat/haproxy-2.0 -y
apt update

# Install
apt -qqy install haproxy=2.0.\*

#EOF
