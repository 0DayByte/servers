#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	ClamAV
#	Debian/Ubuntu
# Ref https://samiux.blogspot.com/2016/08/howto-clamav-099-on-ubuntu-1604-lts.html
###################################################################

# Install ClamAV
apt-get -qqy install clamav clamav-freshclam clamav-daemon libclamunrar7

# Configs
wget -qO /etc/clamav/clamd.conf https://gitlab.com/0DayByte/servers/raw/master/System/Anti.Virus/ClamAV/config/clamd.conf
wget -qO /etc/freshclam.conf https://gitlab.com/0DayByte/servers/raw/master/System/Anti.Virus/ClamAV/config/freshclam.deb.conf

# Enable
systemctl stop clamav-freshclam
systemctl enable clamav-freshclam
systemctl start clamav-freshclam

#EOF