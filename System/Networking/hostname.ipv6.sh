#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#                Setting Hostname
#
###############################################################

# Remove
rm -f /etc/{hosts,hostname}

# Create Files
#touch /etc/{hosts,hostname}

# Set Hostname
cat >/etc/hostname <<EOL
${host}.${domain}
EOL

cat /etc/hostname

# Set Hosts
cat >/etc/hosts <<EOL
127.0.0.1   ${host}.${domain} ${host} localhost localhost.localdomain
${ipv4}  ${host}.${domain} ${host} localhost localhost.localdomain
::1         ${host}.${domain} ${host} localhost localhost.localdomain ip6-localhost ip6-loopback
${ipv6}  ${host}.${domain} ${host} localhost localhost.localdomain
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOL

cat /etc/hosts

#EOF
