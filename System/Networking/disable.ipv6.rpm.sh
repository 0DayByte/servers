#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
# 					Disable IPV6
# 					Red Hat/CentOS
###############################################################

sysctl -w net.ipv6.conf.default.disable_ipv6=1
sysctl -w net.ipv6.conf.all.disable_ipv6=1

#EOF