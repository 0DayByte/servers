#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#                Setting Hostname
#
###############################################################

# Remove
rm -f /etc/{hosts,hostname}

# Create Files
#touch /etc/{hosts,hostname}

# Set Hostname
cat >/etc/hostname <<EOL
${host}.${domain}
EOL

cat /etc/hostname

# Set Hosts
cat >/etc/hosts <<EOL
${host}.${domain}
127.0.0.1		${host}.${domain} ${host} localhost localhost.localdomain localhost4 localhost4.localdomain4
${ip}    ${host}.${domain} ${host} localhost localhost.localdomain localhost4 localhost4.localdomain4
::1             localhost localhost.localdomain localhost6 localhost6.localdomain6
EOL

cat /etc/hosts

#EOF