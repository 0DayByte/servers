#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	Common Packages
#	Red Hat/CentOS
#
###############################################

# Yum Configure
yum -q -y install yum-utils deltarpm

# Add Epel Repo
cd /tmp
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum-config-manager --enable epel

# Add Ghetto Repo
rpm -Uvh http://mirror.ghettoforge.org/distributions/gf/gf-release-latest.gf.el7.noarch.rpm
rm -f /etc/yum.repos.d/gf.repo
wget -O /etc/yum.repos.d/gf.repo https://gitlab.com/0DayByte/servers/raw/master/System/Common/configs/gf.repo

# Add RPMForge Repo
rpm -Uvh http://repository.it4i.cz/mirrors/repoforge/redhat/el7/en/x86_64/rpmforge/RPMS/rpmforge-release-0.5.3-1.el7.rf.x86_64.rpm

# Update
rm -rf /var/cache/yum
yum -q -y update

# Common Packages
yum -q -y install nano wget bind-utils p7zip p7zip-plugins time bzip2 tar ntp curl mailx

#EOF