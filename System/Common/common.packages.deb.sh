#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	Common Packages
#	Debian/Ubuntu
#
###############################################

# Install Common Used Packages
apt-get -qqy install nano wget dnsutils p7zip time bzip2 tar ntp curl software-properties-common dirmngr apt-transport-https ca-certificates zip unzip mailutils

#EOF