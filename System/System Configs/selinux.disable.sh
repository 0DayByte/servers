#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	Disable SE Linux
#
#############################################

sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

#EOF