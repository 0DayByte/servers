#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#         HyperVisor Network Forwarding
#
#############################################

#network_interface='eno1'

##############################################
#
# Forward Networking
#
###############################################

echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
echo "net.ipv6.ip_forward = 1" >> /etc/sysctl.conf
echo "net.ipv4.conf.${network_interface}.forwarding = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.${network_interface}.forwarding = 1" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.forwarding = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.forwarding = 1" >> /etc/sysctl.conf

# Reload System Configs
sysctl --system

#EOF