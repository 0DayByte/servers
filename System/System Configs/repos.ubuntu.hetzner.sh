#!/usr/bin/env bash
set -x #bash verbose


###############################################################
#
# REPO SOURCES
# sets repos for packes
# in sources.list in apt
#
###############################################################
#code_name=bionic

# Remove 
rm /etc/apt/sources.list

# Set Repos
cat >/etc/apt/sources.list <<EOL
######################################
#
#       Hetzner Apt
#       Ubuntu
#
#####################################

deb http://mirror.hetzner.de/ubuntu/packages ${code_name} main restricted universe multiverse
deb http://mirror.hetzner.de/ubuntu/packages ${code_name}-updates main restricted universe multiverse
deb http://mirror.hetzner.de/ubuntu/packages ${code_name}-backports main restricted universe multiverse
deb http://mirror.hetzner.de/ubuntu/packages ${code_name}-security main restricted universe multiverse
EOL