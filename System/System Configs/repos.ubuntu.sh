#!/usr/bin/env bash
set -x #bash verbose


###############################################################
#
# REPO SOURCES
# sets repos for packes
# in sources.list in apt
#
###############################################################

#code_name='bionic'

# Remove 
rm /etc/apt/sources.list

# Set Repos
cat >/etc/apt/sources.list <<EOL
# Ubuntu Main Repo for ${code_name}
deb http://us.archive.ubuntu.com/ubuntu/ ${code_name} universe multiverse main restricted
deb-src http://us.archive.ubuntu.com/ubuntu/ ${code_name} universe multiverse main restricted

# Ubuntu Security Repo for ${code_name}
deb http://security.ubuntu.com/ubuntu ${code_name}-security universe multiverse main restricted
deb-src http://security.ubuntu.com/ubuntu ${code_name}-security universe multiverse main restricted

# Ubuntu Update Repo for ${code_name} (bug fixes and updates)
deb http://us.archive.ubuntu.com/ubuntu/ ${code_name}-updates universe multiverse main restricted
deb-src http://us.archive.ubuntu.com/ubuntu/ ${code_name}-updates universe multiverse main restricted

# Ubuntu Backports Repo for ${code_name}
deb http://us.archive.ubuntu.com/ubuntu/ ${code_name}-backports main restricted universe multiverse
deb-src http://us.archive.ubuntu.com/ubuntu/ ${code_name}-backports main restricted universe multiverse

# Ubuntu Partners Repo
deb http://archive.canonical.com/ubuntu ${code_name} partner
deb-src http://archive.canonical.com/ubuntu ${code_name} partner
EOL

cat /etc/apt/sources.list

# Update and Upgrade
apt-get -qq update
apt-get -qqy dist-upgrade

#EOF