#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	Set System Tweaks
#
#############################################

##############################################
#
# DISABLE IPV6
#
###############################################

echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf
#echo "net.ipv6.conf.${interface}.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.ppp0.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.tun0.disable_ipv6 = 1" >> /etc/sysctl.conf

# Reload System Configs
sysctl --system

#EOF