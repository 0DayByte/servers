#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	Set System Tweaks
#
#############################################

#network_interface='eth0'

##########################################################
#
#swappiness this control is used to define how aggressively 
#the kernel swaps out anonymous memory relative to pagecache and 
#other caches. Increasing the value increases the amount of swapping. 
#The default value is 60.
#
##########################################################

echo "vm.swappiness = 5" >> /etc/sysctl.conf

##########################################################
#
#vfs_cache_pressure this variable controls the tendency of the kernel 
#to reclaim the memory which is used for caching of VFS caches, versus 
#pagecache and swap. Increasing this value increases the rate at which 
#VFS caches are reclaimed.
#
##########################################################

echo "vm.vfs_cache_pressure = 50" >> /etc/sysctl.conf

##############################################
#
# DISABLE IPV6
#
###############################################

echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.${network_interface}.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.ppp0.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.tun0.disable_ipv6 = 1" >> /etc/sysctl.conf

# Reload System Configs
sysctl --system

#EOF