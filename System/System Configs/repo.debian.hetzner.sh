#!/usr/bin/env bash
set -x #bash verbose


###############################################################
#
# REPO SOURCES
# sets repos for packes
# in sources.list in apt
#
###############################################################

# Remove 
rm /etc/apt/sources.list

# Set Repos
cat >/etc/apt/sources.list <<EOL
# Hetzner Debian Repo for ${code_name}
deb http://mirror.hetzner.de/debian/packages ${code_name} main contrib non-free
deb http://mirror.hetzner.de/debian/security ${code_name}/updates main contrib non-free
deb http://mirror.hetzner.de/debian/packages ${code_name}-updates main contrib non-free

# Backports
deb http://ftp.debian.org/debian ${code_name}-backports main
#deb-src http://ftp.debian.org/debian ${code_name}-backports main
EOL

cat /etc/apt/sources.list

# Update and Upgrade
apt-get -qq update
apt-get -qqy dist-upgrade

#EOF