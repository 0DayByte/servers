#!/usr/bin/env bash
set -x #bash verbose


###############################################################
#
# REPO SOURCES
# sets repos for packes
# in sources.list in apt
#
###############################################################

# Remove 
rm /etc/apt/sources.list

# Set Repos
cat >/etc/apt/sources.list <<EOL
# Debian Main Repo for ${code_name}
deb http://ftp.us.debian.org/debian/ ${code_name} main contrib non-free 
deb-src http://ftp.us.debian.org/debian/ ${code_name} main contrib non-free 

# Debian Updates Repo for ${code_name}
deb http://ftp.us.debian.org/debian/ ${code_name}-updates main contrib non-free
deb-src http://ftp.us.debian.org/debian/ ${code_name}-updates main contrib non-free

# Debian Security Updates Repo for ${code_name}
deb http://security.debian.org/ ${code_name}/updates main contrib non-free
deb-src http://security.debian.org/ ${code_name}/updates main contrib non-free

# Backports
deb http://ftp.debian.org/debian ${code_name}-backports main
deb-src http://ftp.debian.org/debian ${code_name}-backports main
EOL

cat /etc/apt/sources.list

# Update and Upgrade
apt-get -qq update
apt-get -qqy dist-upgrade

#EOF