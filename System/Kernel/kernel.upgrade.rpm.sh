#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#                    Kernel Upgrade
#                    Red Hat/CentOS
#
###############################################################

# Add Repo
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
yum -q -y remove kernel-tools*

# Install Kernel
yum --enablerepo=elrepo-kernel -q -y install kernel-ml kernel-ml-headers kernel-ml-tools kernel-ml-tools-libs
yum -q -y remove kernel kernel.x86_64

#EOF