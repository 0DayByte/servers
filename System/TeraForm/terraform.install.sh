#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	Terraform
#
###################################################################

version=0.11.6

# Download / Install
cd /tmp
wget https://releases.hashicorp.com/terraform/${version}/terraform_${version}_linux_amd64.zip
unzip terraform_${version}_linux_amd64.zip
mv terraform /usr/local/bin/
terraform --version

#EOF