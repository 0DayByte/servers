#!/usr/bin/env bash
#set -x #bash verbose

###############################################################
#
#		MediaInfo Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

# Add Repo
cd /tmp
wget https://mediaarea.net/repo/deb/repo-mediaarea-snapshots_1.0-16_all.deb && dpkg -i repo-mediaarea-snapshots_1.0-16_all.deb
apt update
apt upgrade -qqy
apt install mediainfo -qqy

#EOF
