#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		Mopidy Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

# Add Repo
wget -q -O - https://apt.mopidy.com/mopidy.gpg | sudo apt-key add -
wget -q -O /etc/apt/sources.list.d/mopidy.list https://apt.mopidy.com/stretch.list
apt update

# Install
apt-get install mopidy -qqy

# Create User
usermod -g media mopidy

# Configs
#wget /etc/mopidy

# Start
systemctl start mopidy
systemctl enable mopidy

#EOF