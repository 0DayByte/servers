#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#       Sonarr Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

# Add Repo
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 2009837CBFFD68F45BC180471F4F90DE2A9B4BF8
echo "deb https://apt.sonarr.tv/ubuntu bionic main" | sudo tee /etc/apt/sources.list.d/sonarr.list
apt update

# Install
apt install sonarr -qqy

# Add Media Group
systemctl stop sonarr.service

# Create Data Dir
mkdir -p /home/vpn-user/Sonarr
chown vpn-user:media /home/vpn-user/Sonarr

# Configs
systemctl daemon-reload
wget -qO /lib/systemd/system/sonarr.service https://gitlab.com/0DayByte/servers/raw/master/Media/Sonarr/sonarr.service
wget -qO /etc/systemd/system/socat-sonarr.service https://gitlab.com/0DayByte/servers/raw/master/Media/Sonarr/socat-sonarr.service
systemctl enable sonarr.service socat-sonarr.service
systemctl start sonarr.service socat-sonarr.service

# Nginx Reverse Proxy
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/nginx.proxy -O /etc/nginx/snippets/proxy.conf
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Sonarr/sonarr.nginx -O /etc/nginx/sites-available/sonarr

systemctl reload nginx

#EOF