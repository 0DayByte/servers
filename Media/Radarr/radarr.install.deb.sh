#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#       Radarr Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

#radarr_version='0.2.0.1450'

# Make Directories
mkdir -p /home/vpn-user/Radarr
mkdir -p /home/vpn-user/Radarr/backup

# Add Repo
apt install curl -qqy

# Add User
#useradd -s /bin/false -d /opt/Radarr/app -c "Radarr" -g media -u 2004 -r radarr

# Install
cd /home/vpn-user/Radarr
wget https://github.com/Radarr/Radarr/releases/download/v$radarr_version/Radarr.develop.$radarr_version.linux.tar.gz
tar -xvzf Radarr.develop.*.linux.tar.gz
mv Radarr app
chown -R vpn-user:media /home/vpn-user/Radarr/app

# Start
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Radarr/radarr.service -O /etc/systemd/system/radarr.service
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Radarr/socat-radarr.service -O /etc/systemd/system/socat-radarr.service
systemctl daemon-reload
systemctl enable radarr.service socat-radarr.service
systemctl start radarr.service socat-radarr.service
sleep 10
systemctl stop radarr.service
sleep 3
sed -i 's/<LaunchBrowser>True</LaunchBrowser>/<LaunchBrowser>False</LaunchBrowser>/' /home/vpn-user/Radarr/app/.config/Radarr/config.xml
chown vpn-user:media /home/vpn-user/Radarr/app/.config/Radarr/config.xml
systemctl start radarr.service

# Nginx Reverse Proxy
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/nginx.proxy -O /etc/nginx/snippets/proxy.conf
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Radarr/radarr.nginx -O /etc/nginx/sites-available/radarr
cp /etc/nginx/sites-available/radarr /etc/nginx/sites-enabled
systemctl reload nginx

#EOF