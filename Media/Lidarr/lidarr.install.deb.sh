#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		Lidarr Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

#lidarr_version='0.7.1.1381'

# Make Directories
mkdir -p /home/vpn-user/Lidarr/
mkdir -p /home/vpn-user/Lidarr/backups

# Create User
#useradd -s /bin/false -d /opt/Lidarr/app/ -g media -u 2005 -m -r lidarr

# Install
cd /home/vpn-user/Lidarr
wget https://github.com/lidarr/Lidarr/releases/download/v$lidarr_version/Lidarr.master.$lidarr_version.linux.tar.gz
tar -xzvf Lidarr.*.linux.tar.gz
sleep 3
mv /home/vpn-user/Lidarr/Lidarr /home/vpn-user/Lidarr/app

# Set Permission
chown -R vpn-user:media /home/vpn-user/Lidarr/app
chown -R root:root /home/vpn-user/Lidarr/backups
chmod -R 750 /home/vpn-user/Lidarr/app

# Configs
wget -qO /etc/systemd/system/socat-lidarr.service https://gitlab.com/0DayByte/servers/raw/master/Media/Lidarr/socat-lidarr.service
wget -qO /etc/systemd/system/lidarr.service https://gitlab.com/0DayByte/servers/raw/master/Media/Lidarr/lidarr.service 
systemctl daemon-reload
systemctl enable lidarr.service socat-lidarr.service
systemctl start lidarr.service socat-lidarr.service
sleep 10
systemctl stop lidarr.service
sleep 3
sed -i 's/<LaunchBrowser>True</LaunchBrowser>/<LaunchBrowser>False</LaunchBrowser>/' /home/vpn-user/Lidarr/app/.config/Lidarr/config.xml
chown vpn-user:media /home/vpn-user/Lidarr/app/.config/Lidarr/config.xml
systemctl start lidarr.service

# Nginx Reverse Proxy
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/nginx.proxy -O /etc/nginx/snippets/proxy.conf
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Lidarr/lidarr.nginx -O /etc/nginx/sites-available/lidarr
cp /etc/nginx/sites-available/lidarr /etc/nginx/sites-enabled
systemctl reload nginx

#EOF