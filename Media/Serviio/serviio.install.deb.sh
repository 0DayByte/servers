#!/usr/bin/env bash
set -x

#########################################################
#
#       Serviio
#       Debian/Ubuntu
#
#########################################################

#serviio_version='1.10.1'

# Install Requirements
apt-get install net-tools software-properties-common openjdk-8-jre default-jre ffmpeg dcraw wget libavcodec-extra libavformat57 -qqy

# Add User
useradd -r -d /opt/Serviio/app -c "Serviio" -g media -u 2009 serviio

# Download
mkdir -p /opt/Serviio
cd /opt/Serviio
wget -q http://download.serviio.org/releases/serviio-${serviio_version}-linux.tar.gz
sudo tar zxvf serviio-${serviio_version}-linux.tar.gz
mv serviio-${serviio_version} app
rm serviio-${serviio_version}-linux.tar.gz
chown -R serviio:media /opt/Serviio/app
find /opt/Serviio -type f -print0 | xargs -0 chmod 644
find /opt/Serviio -type d -print0 | xargs -0 chmod 755
chmod +x /opt/Serviio/app/bin/{serviio.sh,serviio-console.sh}

# Install
. /opt/Serviio/app/bin/serviio.sh
wget -qO /etc/systemd/system/serviio.service https://gitlab.com/0DayByte/servers/raw/master/Media/Serviio/configs/serviio.service

systemctl daemon-reload
systemctl enable serviio.service
systemctl start serviio.service

#EOF