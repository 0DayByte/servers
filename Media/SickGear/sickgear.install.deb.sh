#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		SickGear Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

# Make Directories
mkdir -p /home/vpn-user/SickGear
mkdir -p /home/vpn-user/SickGear/data
mkdir -p /home/vpn-user/SickGear/backup

# Install Requirements
apt-get install python-cheetah unrar-free git python-lxml python3-lxml python-pip -qqy

# Add User
#useradd -s /bin/false -d /opt/SickGear/app -c "SickGear" -g media -u 2001 -r sickgear

# Install
git clone https://github.com/SickGear/SickGear.git /home/vpn-user/SickGear/app
chown -R vpn-user:media /home/vpn-user/SickGear
chmod -R 770 /home/vpn-user/SickGear
chmod +x /home/vpn-user/SickGear/app/SickBeard.py

# Setup PIP
cd /home/vpn-user/SickGear/app
pip install -r requirements.txt
pip install lxml
pip install regex
pip install scandir

# Configs
wget -qO /home/vpn-user/SickGear/sickgear.upgrade.sh https://gitlab.com/0DayByte/servers/raw/master/Media/SickGear/sickgear.upgrade.sh 
wget -qO /etc/systemd/system/sickgear.service https://gitlab.com/0DayByte/servers/raw/master/Media/SickGear/sickgear.service
wget -qO /etc/systemd/system/socat-sickgear.service  https://gitlab.com/0DayByte/servers/raw/master/Media/SickGear/socat-sickgear.service

# Crontab Upgrade
chmod +x /home/vpn-user/SickGear/sickgear.upgrade.sh
#echo "0 4 20 * * /home/vpn-user/SickGear/sickgear.upgrade.sh" | ssh $i " tee -a /var/spool/cron/root"

# Config
systemctl daemon-reload
systemctl enable sickgear.service socat-sickgear.service
systemctl start sickgear.service socat-sickgear.service
sleep 10
systemctl stop sickgear.service
sleep 3
sed -i 's/0.0.0.0/127.0.0.1/' /opt/SickGear/data/config.ini
sed -i 's/handle_reverse_proxy = 0/handle_reverse_proxy = 1/' /opt/SickGear/data/config.ini
chown sickgear:media /home/vpn-user/SickGear/data/config.ini
chmod 640 /home/vpn-user/SickGear/data/config.ini
systemctl start sickgear

# Nginx Reverse Proxy
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/nginx.proxy -O /etc/nginx/snippets/proxy.conf
wget https://gitlab.com/0DayByte/servers/raw/master/Media/SickGear/sickgear.nginx -O /etc/nginx/sites-available/sickgear
cp /etc/nginx/sites-available/sickgear /etc/nginx/sites-enabled
systemctl reload nginx

#EOF