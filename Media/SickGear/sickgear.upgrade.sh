# Set Source Directory of Bash scripts

######################################
#
#	SickGear Upgrade
#
######################################

# Echo Start
#echo "Starting with SickGear Upgrade"

# Start
systemctl stop sickgear

# - Set Needed Input
now=`date +"%m-%d-%Y"`

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# Backup
mkdir -p "$DIR/backups"
cd "$DIR"
cd ..
zip -qr SickGear-$now.zip SickGear -x *backups*
mv SickGear-*.zip "$DIR/backups"
find "${DIR}/backups/"SickGear-*.zip -mtime +93 -exec rm {} \;

# Problems Pulling new branch
#rm -rf "$DIR/app"
#git clone https://github.com/SickGear/SickGear.git -b develop "$DIR/app"
#git branch --set-upstream-to=origin/develop pull/857/feature/ChangeUI

# Upgrade
cd "$DIR/app"
git pull

# Permissions
chown -R vpn-user:media "$DIR"
chown -R root:root "$DIR/backups"
chown -R root:root "$DIR/sickgear.upgrade.sh"
find "$DIR" -type f -print0 | xargs -0 chmod 2640
find "$DIR" -type d -print0 | xargs -0 chmod 2750
chmod 640 "$DIR/sickgear.upgrade.sh"
chmod +x "$DIR/sickgear.upgrade.sh"

# Start
systemctl start sickgear

# Echo Done
#echo "Finished with SickGear Upgrade"

#EOF
