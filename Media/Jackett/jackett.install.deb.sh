#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#       Jackett Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

#jackett_version='0.12.1638'

# Make Directories
mkdir -p /home/vpn-user/Jackett
mkdir -p /home/vpn-user/Jackett/{backups,data}

# Add Repo
apt install libicu60 openssl1.0 bzip2 -qqy

# Add User
#useradd -s /bin/false -d /opt/Jackett/app -c "Jackett" -g media -u 2007 -r jackett

# Install
cd /home/vpn-user/Jackett
wget https://github.com/Jackett/Jackett/releases/download/v$jackett_version/Jackett.Binaries.LinuxAMDx64.tar.gz
tar -xvzf Jackett.Binaries.LinuxAMDx64.tar.gz
mv Jackett app
chown -R vpn-user:media /home/vpn-user/Jackett

# Start
wget -qO /etc/systemd/system/jackett.service https://gitlab.com/0DayByte/servers/raw/master/Media/Jackett/jackett.service
wget -qO /etc/systemd/system/socat-jackett.service https://gitlab.com/0DayByte/servers/raw/master/Media/Jackett/socat-jackett.service
systemctl daemon-reload
systemctl enable jackett.service socat-jackett.service
systemctl start jackett.service socat-jackett.service

# Nginx Reverse Proxy
wget -qO /etc/nginx/snippets/proxy.conf https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/nginx.proxy
wget  -qO /etc/nginx/sites-available/jackett https://gitlab.com/0DayByte/servers/raw/master/Media/Jackett/jackett.nginx
cp /etc/nginx/sites-available/jackett /etc/nginx/sites-enabled
systemctl reload nginx

#EOF