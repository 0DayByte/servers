#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#       CouchPotato Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

# Make Directories
mkdir -p /home/vpn-user/CouchPotato
mkdir -p /home/vpn-user/CouchPotato/movies
mkdir -p /home/vpn-user/CouchPotato/archive
mkdir -p /home/vpn-user/CouchPotato/backup

# Install Requirements
apt-get install unrar-free git python-lxml python3-lxml python-pip -qqy

# Add User
#useradd -s /bin/false -d /opt/CouchPotato/movies/app -c "CouchPotato" -g media -u 2002 -r couchpotato
#useradd -s /bin/false -d /opt/CouchPotato/archive/app -c "CouchPotato Archive" -g media -u 2003 -r couchpotato-archive

# Install
git clone https://github.com/CouchPotato/CouchPotatoServer.git /home/vpn-user/CouchPotato/movies/app
git clone https://github.com/CouchPotato/CouchPotatoServer.git /home/vpn-user/CouchPotato/archive/app
chown -R vpn-user:media /home/vpn-user/CouchPotato/movies/app
chown -R vpn-user:media /home/vpn-user/CouchPotato/archive/app

# Setup PIP
pip install --upgrade pyopenssl

# Configs
wget https://gitlab.com/0DayByte/servers/raw/master/Media/CouchPotato/couchpotato.upgrade.sh -O /home/vpn-user/CouchPotato/couchpotato.upgrade.sh
wget https://gitlab.com/0DayByte/servers/raw/master/Media/CouchPotato/couchpotato-archive.service -O /etc/systemd/system/couchpotato-archive.service
wget https://gitlab.com/0DayByte/servers/raw/master/Media/CouchPotato/couchpotato.service -O /etc/systemd/system/couchpotato.service
wget https://gitlab.com/0DayByte/servers/raw/master/Media/CouchPotato/socat-couchpotato.service -O /etc/systemd/system/socat-couchpotato.service
wget https://gitlab.com/0DayByte/servers/raw/master/Media/CouchPotato/socat-couchpotato-archive.service -O /etc/systemd/system/socat-couchpotato-archive.service

# Crontab Upgrade
chmod +x /home/vpn-user/CouchPotato/couchpotato.upgrade.sh
#echo "10 4 20 * * /opt/CouchPotato/couchpotato.upgrade.sh" | ssh $i " tee -a /var/spool/cron/root"

# Start
systemctl daemon-reload
systemctl enable couchpotato.service socat-couchpotato.service couchpotato-archive.service socat-couchpotato-archive.service
systemctl start couchpotato.service socat-couchpotato.service couchpotato-archive.service socat-couchpotato-archive.service
sleep 10
systemctl stop couchpotato couchpotato-archive
sleep 3
sed -i 's/port = 5050/port = 5051/' /home/vpn-user/CouchPotato/archive/app/.couchpotato/settings.conf
sed -i 's/permission_file = 0644/permission_file = 0664/' /home/vpn-user/CouchPotato/movies/app/.couchpotato/settings.conf
sed -i 's/permission_file = 0644/permission_file = 0664/' /home/vpn-user/CouchPotato/archive/app/.couchpotato/settings.conf
sed -i 's/permission_folder = 0755/permission_folder = 0775/' /home/vpn-user/CouchPotato/movies/app/.couchpotato/settings.conf
sed -i 's/permission_folder = 0755/permission_folder = 0775/' /home/vpn-user/CouchPotato/archive/app/.couchpotato/settings.conf
sed '/^port = 5050/a host = 127.0.0.1' /home/vpn-user/CouchPotato/movies/app/.couchpotato/settings.conf
sed '/^port = 5051/a host = 127.0.0.1' /home/vpn-user/CouchPotato/archive/app/.couchpotato/settings.conf
sed -i 's:url_base =  :url_base =  /:' /home/vpn-user/CouchPotato/movies/app/.couchpotato/settings.conf
sed -i 's:url_base =  :url_base =  /:' /home/vpn-user/CouchPotato/archive/app/.couchpotato/settings.conf
chown vpn-users:media /home/vpn-user/CouchPotato/movies/app/.couchpotato/settings.conf
chown vpn-user:media /home/vpn-user/CouchPotato/movies/app/.couchpotato/settings.conf
systemctl start couchpotato couchpotato-archive socat-couchpotato socat-couchpotato-archive

# Nginx Reverse Proxy
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/nginx.proxy -O /etc/nginx/snippets/proxy.conf
wget https://gitlab.com/0DayByte/servers/raw/master/Media/CouchPotato/couchpotato.nginx -O /etc/nginx/sites-available/movies
cp /etc/nginx/sites-available/movies /etc/nginx/sites-enabled
systemctl reload nginx

#EOF