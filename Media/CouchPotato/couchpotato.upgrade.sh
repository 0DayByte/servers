# Set Source Directory of Bash scripts

######################################
#
#	CouchPotato Upgrade
#
######################################

# Echo Start
echo "Starting with CouchPotato Upgrade"

# Start
systemctl stop couchpotato couchpotato-archive

# - Set Needed Input
now=`date +"%m-%d-%Y"`
#
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# Backup
mkdir -p "$DIR/backups"
cd "$DIR"
cd ..
zip -r CouchPotato-$now.zip CouchPotato -x *backups*
mv CouchPotato-*.zip "$DIR/backups"
find "${DIR}/backups/"CouchPotato-*.zip -mtime +93 -exec rm {} \;

# Upgrade Movies
cd "$DIR/movies/app"
#git reset --hard HEAD
git pull

# Upgrade Archive
cd "$DIR/archive/app"
#git reset --hard HEAD
git pull

# Permissions
chown -R vpn-user:media "$DIR"
chown -R root:root $DIR/backups $DIR/couchpotato.upgrade.sh
find "$DIR" -type f -print0 | xargs -0 chmod 2640
find "$DIR" -type d -print0 | xargs -0 chmod 2750
chmod 750 "$DIR"
chmod +x "$DIR/couchpotato.upgrade.sh"
chmod +x "$DIR/movies/app/CouchPotato.py"
chmod +x "$DIR/archive/app/CouchPotato.py"

# Start
systemctl start couchpotato couchpotato-archive

# Echo Done
echo "Finished with CouchPotato Upgrade"

#EOF