#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		Plex Install
#       Debian / Ubuntu
#
###############################################################

# Repos
echo deb https://downloads.plex.tv/repo/deb public main | sudo tee /etc/apt/sources.list.d/plexmediaserver.list
curl https://downloads.plex.tv/plex-keys/PlexSign.key | apt-key add -

# Install
apt-get update -qq
apt-get install plexmediaserver -qqy

# Intial setup requires local connection only. On install of remote server run ssh tunnel on client to access localhost on remote server. 
# ssh plex.media.server -L 32400:localhost:32400
# Now access open browser and navigate to http://localhost:32400

#EOF