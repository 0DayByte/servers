#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#        Media Server Install on 
#        KVM Ubuntu (run time 7 mins)
#            JellyFin/Serviio
#          SickGear/Couchpotato
#        Sonarr/Radarr/Lidarr/Jackett
#
###############################################################
###############################################################
#
#   VARIABLES
#hostname='media'
#domain='example.com'
#packages='deb'
#distro='ubuntu'
#distro_version='18.04'
#code_name='bionic'
#network_interface='eth0'
#cerbot_email='admin@example.com'
#fail2ban_ignore_ip='123.123.123.123 234.234.234.234'
#mariadb_version='10.4'
#mariadb_root_pass='P@55w0rd'
#filebrowser_version='2.0.16'
#deluge_home='/home/vpn-user/Deluge'
#radarr_version='0.2.0.1450'
#lidarr_version='0.7.1.1381'
#jackett_version='0.12.1638'
#pia_username=''
#pia_pass
#serviio_version='1.10.1'
#
#
###############################################################
###############################################################

echo "Starting with installing media server on ${hostname} ${distro} ${distro_version}"

# Make Install Directory
cd /usr/src/
mkdir -p install
cd install

# Set Source Directory of Bash scripts
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# -Set permissions for scripts
chown -R root:root $DIR
chmod -R 700 $DIR

# Update
apt-get -q update
apt-get -qqy install locales
echo en_US UTF-8 >> /etc/locale.gen
locale-gen
apt-get -qqy dist-upgrade
apt-get -qqy autoremove
apt-get -q autoclean
apt-get -q clean
apt-get -qqy install ca-certificates
#cp /usr/share/zoneinfo/America/New_York /etc/localtime

################# Install

# Add Media Group
groupadd -g 2000 media

# Add User
useradd -d /home/vpn-user -c "VPN User" -g media -u 2006 vpn-user

# HWE Kernel
#apt install --install-recommends linux-generic-hwe-18.04 -qqy
#apt install remove linux-generic-18.04 -qqy

# Network Forwarding
mkdir -p $DIR/system
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.conf.network.forwarding.deb.sh
. sysctl.conf.network.forwarding.deb.sh

# System Configs
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.ipv6.conf.sh
. sysctl.ipv6.conf.sh

##################################
#
# Networking
#
##################################

# OpenSSH
mkdir -p $DIR/system
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/openssh.install.sh
. openssh.install.sh

# Iptables
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Networking/IPTables/iptables.install.sh
. iptables.install.sh
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/iptables/rules.v4 -O /etc/iptables/rules.v4
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/iptables/rules.v6 -O /etc/iptables/rules.v6
iptables-restore < /etc/iptables/rules.v4
ip6tables-restore < /etc/iptables/rules.v6

# NameSpaced OpenVPN
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Networking/OpenVPN/namespaced.openvpn.install.sh
. namespaced.openvpn.install.sh

# PIA
mkdir -p /etc/openvpn/pia
wget -q -qO /etc/openvpn/pia/login https://gitlab.com/0DayByte/servers/raw/master/Networking/OpenVPN/config/pia/pia.login
wget -q -qO /etc/openvpn/pia/ca.rsa.2048.crt https://gitlab.com/0DayByte/servers/raw/master/Networking/OpenVPN/config/pia/ca.rsa.2048.crt
wget -q -qO /etc/openvpn/pia/crl.rsa.2048.pem https://gitlab.com/0DayByte/servers/raw/master/Networking/OpenVPN/config/pia/crl.rsa.2048.pem
wget -q -qO /etc/openvpn/client/ca-toronto.conf https://gitlab.com/0DayByte/servers/raw/master/Networking/OpenVPN/config/pia/ca-toronto.conf

##################################
#
# System Applications
#
##################################

# Fail2Ban
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Networking/Fail2Ban/fail2ban.install.sh
. fail2ban.install.sh

# Postfix
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/postfix.install.sh
. postfix.install.sh

# SSL
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/openssl.install.sh
. openssl.install.sh

# Webmin
mkdir -p $DIR/webmin
cd $DIR/webmin
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/Webmin/webmin.install.sh
. webmin.install.sh

# Nginx
mkdir -p $DIR/webserver
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/nginx.install.sh
. nginx.install.sh

# MariaDB
mkdir -p $DIR/database
cd $DIR/database
wget -q https://gitlab.com/0DayByte/servers/raw/master/Databases/MariaDB/mariadb.install.sh
. mariadb.install.sh

##################################
#
# File Shares
#
##################################

# Samba
mkdir -p $DIR/fileserver
cd $DIR/fileserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/FileServer/Samba/samba.intall.deb.sh
. samba.intall.deb.sh

# VSFTPD
cd $DIR/fileserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/FileServer/Vsftp/vsftpd.install.deb.sh
. vsftpd.install.deb.sh

# NFS
cd $DIR/fileserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/FileServer/NFS/nfs.install.sh
. nfs.install.sh

# File Browser
cd $DIR/fileserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/FileBrowser/filebrowser.install.deb.sh
. filebrowser.install.deb.sh

# Deluge
cd $DIR/fileserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/Networking/Deluge/deluged.install.ubuntu.sh
. deluged.install.ubuntu.sh

##################################
#
# Media Management
#
##################################

# SickGear
mkdir -p $DIR/media
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/SickGear/sickgear.install.deb.sh
. sickgear.install.deb.sh

# Couchpotato
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/CouchPotato/couchpotato.install.deb.sh
. couchpotato.install.deb.sh

# Mono (Sonarr/Lidarr/Radarr) 
apt install gnupg ca-certificates -qqy
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | tee /etc/apt/sources.list.d/mono-official-stable.list
apt update
apt install libmono-cil-dev -qqy

# MediaInfo
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/MediaInfo/mediainfo.install.deb.sh
. mediainfo.install.deb.sh

# Sonarr
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Sonarr/sonarr.install.deb.sh
. sonarr.install.deb.sh

# Radarr
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Radarr/radarr.install.deb.sh
. radarr.install.deb.sh

# Lidarr
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Lidarr/lidarr.install.deb.sh
. lidarr.install.deb.sh

# Jackett
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Jackett/jackett.install.deb.sh
. jackett.install.deb.sh

# Mopidy + MPDroid | ncmpcpp | mpd web interface
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Mopidy/mopidy.install.deb.sh
. mopidy.install.deb.sh

# Ombi
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Ombi/ombi.install.deb.sh
. ombi.install.deb.sh

# Organizr

##################################
#
# Media Servers
#
##################################

# JellyFin
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/JellyFin/jellyfin.install.deb.sh
. jellyfin.install.deb.sh

# Serviio
#cd $DIR/media
#wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Serviio/serviio.install.deb.sh
#. serviio.install.deb.sh

# Plex Server
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Plex/plex.install.sh
. plex.install.sh

echo "Finshed with installing media server on ${hostname} ${distro} ${distro_version}"

#EOF