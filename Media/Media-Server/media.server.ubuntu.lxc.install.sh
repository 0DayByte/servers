#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#        Media Server Install on 
#        KVM Ubuntu (run time 7 mins)
#            JellyFin/Serviio
#          SickGear/Couchpotato
#        Sonarr/Radarr/Lidarr/Jackett
#
###############################################################
###############################################################
#
#   VARIABLES
hostname='subdomain'
domain='example.com'
packages='deb'
distro='ubuntu'
distro_version='18.04'
code_name='bionic'
mariadb_version='10.5'
mariadb_root_pass='P@55w0rd'
filebrowser_version='2.10.0'
media_user='media'
media_group='media'
#
#
###############################################################
###############################################################

echo "Starting with lxc installing of Media Server on ${hostname} ${distro} ${distro_version}"

# Make Install Directory
cd /usr/src/
mkdir -p install
cd install

# Set Source Directory of Bash scripts
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# -Set permissions for scripts
chown -R root:root $DIR
chmod -R 700 $DIR

# Update
apt-get -q update
apt-get -qqy install locales
echo en_US UTF-8 >> /etc/locale.gen
locale-gen
apt-get -qqy dist-upgrade
apt-get -qqy autoremove
apt-get -q autoclean
apt-get -q clean
apt-get -qqy install ca-certificates
#cp /usr/share/zoneinfo/America/New_York /etc/localtime

################# Install

# Add Media Group
groupadd -g 1200 ${media_group}

# Add User
useradd  -c "Media User" -g ${media_group} -d /home/Media -m -u 1200 ${media_user}

##################################
#
# Networking
#
##################################

# OpenSSH
mkdir -p $DIR/system
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/openssh.install.sh
. openssh.install.sh

##################################
#
# System Applications
#
##################################

# Postfix
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/postfix.install.sh
. postfix.install.sh

# SSL
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/openssl.install.sh
. openssl.install.sh

# Webmin
mkdir -p $DIR/webmin
cd $DIR/webmin
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/Webmin/webmin.install.sh
. webmin.install.sh

# Nginx
mkdir -p $DIR/webserver
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/nginx.install.sh
. nginx.install.sh

# MariaDB
mkdir -p $DIR/database
cd $DIR/database
wget -q https://gitlab.com/0DayByte/servers/raw/master/Databases/MariaDB/mariadb.install.sh
. mariadb.install.sh

##################################
#
# File Shares
#
##################################

# VSFTPD
cd $DIR/fileserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/FileServer/Vsftp/vsftpd.install.deb.sh
. vsftpd.install.deb.sh

# File Browser
cd $DIR/fileserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/FileBrowser/filebrowser.install.deb.sh
. filebrowser.install.deb.sh

##################################
#
# Media Management
#
##################################

# MediaInfo
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/MediaInfo/mediainfo.install.deb.sh
. mediainfo.install.deb.sh

# Mopidy + MPDroid | ncmpcpp | mpd web interface
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Mopidy/mopidy.install.deb.sh
. mopidy.install.deb.sh

# Organizr

##################################
#
# Media Servers
#
##################################

# JellyFin
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/JellyFin/jellyfin.install.deb.sh
. jellyfin.install.deb.sh

# Plex Server
cd $DIR/media
wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Plex/plex.install.sh
. plex.install.sh

echo "Finshed with installing media server on ${hostname} ${distro} ${distro_version}"

#EOF