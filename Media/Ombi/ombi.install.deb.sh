#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		Ombi Install
#       Debian / Ubuntu
#       ref.
#           https://github.com/tidusjar/Ombi
#
###############################################################
###############################################################

# Add Repo
wget -qO - https://repo.ombi.turd.me/pubkey.txt | sudo apt-key add -
echo "deb [arch=amd64] http://repo.ombi.turd.me/stable/ jessie main" | sudo tee "/etc/apt/sources.list.d/ombi.list"
apt update

# Install
apt install ombi -qqy

# Nginx Reverse Proxy
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/nginx.proxy -O /etc/nginx/snippets/proxy.conf
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Ombi/ombi.nginx -O /etc/nginx/sites-available/ombi
cp /etc/nginx/sites-available/ombi /etc/nginx/sites-enabled
systemctl reload nginx

#EOF