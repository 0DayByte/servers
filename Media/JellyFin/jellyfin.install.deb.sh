#!/usr/bin/env bash
#set -x #bash verbose

###############################################################
#
#		JellyFin Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

# Install Requirements
apt-get install apt-transport-https -qqy

# Add Repo
wget -O - https://repo.jellyfin.org/debian/jellyfin_team.gpg.key | sudo apt-key add -
touch /etc/apt/sources.list.d/jellyfin.list
echo "deb [arch=amd64] https://repo.jellyfin.org/ubuntu bionic main" | sudo tee /etc/apt/sources.list.d/jellyfin.list
apt update

# Install
apt install jellyfin -qqy

# Add Media Group
systemctl stop jellyfin.service
usermod -g media jellyfin
wget https://gitlab.com/0DayByte/servers/raw/master/Media/JellyFin/jellyfin -O /etc/default/jellyfin

# Start
systemctl daemon-reload
systemctl enable jellyfin.service
systemctl start jellyfin.service

#EOF