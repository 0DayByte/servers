#!/usr/bin/env bash

#########################################################
#
#       MP4 Automator
#       Debian/Ubuntu
#
#########################################################



apt-get install python-pip git -qqy

pip install requests\
    && pip install requests[security]\
    && pip install python-dateutil\
    && pip install requests-cache\
    && pip install babelfish\
    && pip install tmdbsimple\
    && pip install deluge-client\
    && pip install qtfaststart\
    && pip install gevent\
    && pip install stevedore==1.19.1\
    && pip install "guessit<2"\
    && pip install "subliminal<2"

# Setup
mkdir -p /opt/MP4.Automator
mkdir -p /opt/MP4.Automator/{tv,movies,movies-archive}
git clone https://github.com/mdhiggins/sickbeard_mp4_automator.git /opt/MP4.Automator/app
rm /opt/MP4.Automator/app/autoProcess.ini
ln -s /opt/MP4.Automator/app/* /opt/MP4.Automator/tv
ln -s /opt/MP4.Automator/app/* /opt/MP4.Automator/movies
ln -s /opt/MP4.Automator/app/* /opt/MP4.Automator/movies-archive
