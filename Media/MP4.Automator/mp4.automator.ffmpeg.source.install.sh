
tmpDIR="/usr/src/ffmpeg"


apt-get install autoconf automake build-essential checkinstall git libass-dev libfaac-dev\
  libgpac-dev libjack-jackd2-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libopus-dev\
  librtmp-dev libsdl1.2-dev libspeex-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev\
  libx11-dev libxext-dev libxfixes-dev pkg-config texi2html zlib1g-dev libaom-dev libx264-dev libx265-dev\
  libfdk-aac-dev libvpx-dev -y


mkdir /usr/src/ffmpeg
cd /usr/src/ffmpeg
mkdir "$HOME/ffmpeg_build"
git clone --depth 1 git://source.ffmpeg.org/ffmpeg || ( echo 'Check internet connection...' && exit 1 )
cd ffmpeg
PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig"
./configure --prefix="$HOME/ffmpeg_build" \
  --extra-cflags="-I$HOME/ffmpeg_build/include" --extra-ldflags="-L$HOME/ffmpeg_build/lib"\
  --bindir="$HOME/bin" --extra-libs="-ldl" --enable-gpl --enable-libass --enable-libopus\
  --enable-libfdk-aac --enable-libmp3lame --enable-libopencore-amrnb --enable-libopencore-amrwb\
  --enable-libspeex --enable-librtmp --enable-libtheora --enable-libvorbis --enable-libvpx\
  --enable-libx264 --enable-nonfree --enable-version3 
make
mv ffmpeg /usr/bin/ffmpeg-mp4automator
mv ffprobe /usr/bin/ffprobe-mp4automator
rm -rf $tmpDIR