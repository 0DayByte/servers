#!/usr/bin/env bash
set -x #bash verbose
echo "Starting of Zulip Chat Install"

############################################################
#
#		Zulip Chat Install
#		Debian 8 / Ubuntu Xenial 
#		(Bionic and farther will not work)
#       (has to be ran by hand, can't run in script)
#
############################################################

#zulip_subdomain='chat'
#domain='example.com'
#zulip_version='1.8.1'
#zulip_email=admin@example.com

# Requirements
apt-get -qqy install python-dev python-pip openssl nano vim wget openssl

# Download
cd /usr/src/
wget https://www.zulip.org/dist/releases/zulip-server-latest.tar.gz
tar -xvzf zulip-server-latest.tar.gz
rm zulip-server-latest.tar.gz
cd zulip-server-${zulip_version}
. /scripts/setup/install --hostname=${zulip_subdomain}.${domain} --email=${zulip_email} --certbot

echo "Finished with Zulip Chat Install"