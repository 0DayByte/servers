#!/usr/bin/env bash

##############################################
#
#   Rocket.Chat Install
#
###############################################

wget https://gitlab.com/0DayByte/servers/-/raw/master/Communications/RocketChat/rocket.chat.install.${packages}.sh
. rocket.chat.install.${packages}.sh

#EOF