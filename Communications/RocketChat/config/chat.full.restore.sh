#!/usr/bin/env bash
set -x #bash verbose

###########################################################
#
# Rocket Chat Full Restore
# 
###########################################################
###########################################################
#
files='/opt/'
archive='/backups/archive'
date=`01-01-01`
#
###########################################################

echo "Starting with Full Restore of RocketChat Server"

# Prep
cd ${archive}
unzip -q roguerater-backup-${date}.zip

# Restore Database
mkdir -p ${backup}/roguerater-backup/mongo-database
mongorestore --db rocketchat ${archive}/roguerater-backup/mongo-database

# Restore App and Scripts
rsync -av ${archive}/roguerater-backup/Rocket.Chat ${files}
rsync -av ${archive}/roguerater-backup/scripts ${files}

# Restore Configs
rsync -av ${archive}/roguerater-backup/configs/nginx/nginx.conf /etc/nginx
rsync -av ${archive}/roguerater-backup/configs/nginx/{sites-available,sites-enabled,snippets} /etc/nginx
rsync -av ${archive}/roguerater-backup/configs/netplan /etc 
rsync -av ${archive}/roguerater-backup/configs/letsencrypt /etc
rsync -av ${archive}/roguerater-backup/configs/mongod.conf /etc
rsync -av ${archive}/roguerater-backup/configs/postfix /etc
rsync -av ${archive}/roguerater-backup/configs/opendkim.conf /etc
rsync -av ${archive}/roguerater-backup/configs/opendkim /etc
rsync -av ${archive}/roguerater-backup/configs/ssh /etc

# Cleanup
rm -rf ${archive}/roguerater-backup

echo "Finished with Full Restore of RocketChat Server"
echo "System will Reboot in 10 secs"
sleep 10
reboot

#EOF