##!/usr/bin/env bash
set -x #bash verbose

###########################################################
#
# Rocket Chat Upgrade
# 
###########################################################

echo "Starting with Backup of RocketChat"

files='/opt/'
backup='/backup'
now=`date +"%m-%d-%Y"`

echo "Starting with Backup of RocketChat"

# Prep
mkdir -p ${backup}
mkdir -p ${backup}/archive
mkdir -p ${backup}/roguerater-backup
systemctl stop rocketchat nginx
sleep 2

# Backup Database
mkdir -p ${backup}/roguerater-backup/mongo-database
mongodump -o ${backup}/roguerater-backup/mongo-database

# Backup App and Scripts
rsync -av ${files}/* ${backup}/roguerater-backup/

# Backup Configs
mkdir -p ${backup}/roguerater-backup/configs
mkdir -p ${backup}/roguerater-backup/configs/nginx
rsync -av /etc/nginx/nginx.conf ${backup}/roguerater-backup/configs/nginx
rsync -av /etc/nginx/{sites-available,sites-enabled,snippets} ${backup}/roguerater-backup/configs/nginx
rsync -av /etc/netplan ${backup}/roguerater-backup/configs/
rsync -av /etc/letsencrypt ${backup}/roguerater-backup/configs/
rsync -av /etc/mongod.conf ${backup}/roguerater-backup/configs/
rsync -av /etc/postfix ${backup}/roguerater-backup/configs/
rsync -av /etc/opendkim.conf ${backup}/roguerater-backup/configs/
rsync -av /etc/opendkim ${backup}/roguerater-backup/configs/
rsync -av /etc/ssh ${backup}/roguerater-backup/configs/

# Compress
cd ${backup}
zip -r roguerater-backup-${now}.zip roguerater-backup
mv *.zip archive
rm -rf roguerater-backup

echo "Finished with Backup of RocketChat"
echo "Starting with Upgrade of RocketChat"

# Upgrade
rm -rf ${files}/Rocket.Chat
curl -L https://releases.rocket.chat/latest/download -o /tmp/rocket.chat.tgz
tar -xzf /tmp/rocket.chat.tgz -C /tmp
cd /tmp/bundle/programs/server && npm install
mv /tmp/bundle ${files}/Rocket.Chat
chown -R rocketchat:rocketchat ${files}/Rocket.Chat

apt update -qq
apt dist-upgrade -qqy
apt autoremove -qqy

echo "Finished with Upgrade of RocketChat"
echo "System will Reboot in 10 secs"

sleep 10

reboot

echo "Finished with Upgrade of RocketChat"

#EOF