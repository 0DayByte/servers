##!/usr/bin/env bash
set -x #bash verbose

###########################################################
#
# Rocket Chat Backup
# 
###########################################################

echo "Starting with Backup of RocketChat"

files='/opt'
backup='/backup'
now=`date +"%m-%d-%Y"`

# Prep
mkdir -p ${backup}
mkdir -p ${backup}/archive
mkdir -p ${backup}/roguerater-backup
systemctl stop rocketchat nginx
sleep 2

# Backup Database
mkdir -p ${backup}/roguerater-backup/mongo-database
mongodump -o ${backup}/roguerater-backup/mongo-database

# Backup App and Scripts
rsync -av ${files}/* ${backup}/roguerater-backup/

# Backup Configs
mkdir -p ${backup}/roguerater-backup/configs
mkdir -p ${backup}/roguerater-backup/configs/nginx
rsync -av /etc/nginx/nginx.conf ${backup}/roguerater-backup/configs/nginx
rsync -av /etc/nginx/{sites-available,sites-enabled,snippets} ${backup}/roguerater-backup/configs/nginx
rsync -av /etc/netplan ${backup}/roguerater-backup/configs/
rsync -av /etc/letsencrypt ${backup}/roguerater-backup/configs/
rsync -av /etc/mongod.conf ${backup}/roguerater-backup/configs/
rsync -av /etc/postfix ${backup}/roguerater-backup/configs/
rsync -av /etc/opendkim.conf ${backup}/roguerater-backup/configs/
rsync -av /etc/opendkim ${backup}/roguerater-backup/configs/
rsync -av /etc/ssh ${backup}/roguerater-backup/configs/

# Compress
cd ${backup}
zip -r roguerater-backup-${now}.zip roguerater-backup
mv *.zip archive
rm -rf roguerater-backup

# Post
systemctl start rocketchat nginx

echo "Finish with Backup of RocketChat"

#EOF