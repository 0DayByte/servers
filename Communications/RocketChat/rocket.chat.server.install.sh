#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#	Rocket.Chat Server Install on Ubuntu Bionic (run time 7 mins)
#	MongoDB,NodeJS,Nginx,Postfix
#
###############################################################
###############################################################
#
#   VARIABLES
#hostname='chat'
#domain='example.com'
#packages=deb
#distro=debian
#distro_version=9
#code_name=stretch
#netplan_file='50-cloud-init'
#network_interface='enp4s0'
#ip4_address='192.168.1.5/24'
#ip4_gateway='192.168.1.1'
#ip6_address='2345:6n90:40::4/48'
#ip6_gateway='2345:6n90:40::1'
#ip4_dns1='192.168.1.1'
#ip4_dns2='1.1.1.1'
#ip6_dns1=2345:6n90:40::1'
#ip6_dns2='2606:4700:4700::1111'
#cerbot_email='hostmaster@example.com'
#
###############################################################
###############################################################

echo "starting with ${hostname} install on ${distro} ${distro_version}"

# Make Install Directory
cd /usr/src/
mkdir -p ${hostname}-install
cd ${hostname}-install

# Set Source Directory of Bash scripts
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# -Set permissions for scripts
chown -R root:root /usr/src/${hostname}-install
chmod -R 700 /usr/src/${hostname}-install

# Update
apt-get -q update
#apt-get -qqy install locales
#echo en_US UTF-8 >> /etc/locale.gen
#locale-gen
apt-get -qqy dist-upgrade
apt-get -qqy autoremove
apt-get -q autoclean
apt-get -q clean
#dpkg-reconfigure tzdata

############################# Install ########################

# System Configs
mkdir $DIR/system
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.ipv6.conf.sh
. sysctl.ipv6.conf.sh

# Apt
#cd $DIR/system
#wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/repos.${distro}.sh
#. repos.${distro}.sh

# Common Packages
#cd $DIR/system
#wget https://gitlab.com/0DayByte/servers/raw/master/System/Common/common.packages.deb.sh
#. common.packages.deb.sh

# OpenSSH
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/openssh.install.sh
. openssh.install.sh

# Iptables
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/IPTables/iptables.install.sh
. iptables.install.sh
wget -qO /etc/iptables/rules.v4 https://gitlab.com/0DayByte/servers/raw/master/Webserver/Drupal%20Server/config/iptables/rules.v4 
wget -qO /etc/iptables/rules.v6 https://gitlab.com/0DayByte/servers/raw/master/Webserver/Drupal%20Server/config/iptables/rules.v6
iptables-restore < /etc/iptables/rules.v4
ip6tables-restore < /etc/iptables/rules.v6

# Fail2Ban
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/Fail2Ban/fail2ban.install.sh
. fail2ban.install.sh

# SSL
mkdir cd $DIR/webserver
cd $DIR/webserver
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/openssl.install.sh
. openssl.install.sh

# Nginx
cd $DIR/webserver
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/nginx.install.sh
. nginx.install.sh

# Certbot
#cd $DIR/webserver
#wget https://gitlab.com/0DayByte/servers/-/raw/master/Websites/Lets.Encrypt/certbot.install.sh
#. certbot.install.sh

# Postfix
mkdir cd $DIR/mail
cd $DIR/mail
wget https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/postfix.install.sh
. postfix.install.sh

# OpenDKIM
cd $DIR/mail
wget https://gitlab.com/0DayByte/servers/-/raw/master/Mail/OpenDKIM/opendkim.install.sh
. opendkim.install.sh

# Rocket.Chat
mkdir $DIR/rocket.chat
cd $DIR/rocket.chat
wget https://gitlab.com/0DayByte/servers/-/raw/master/Communications/RocketChat/rocket.chat.install.sh
. rocket.chat.install.sh

#EOF
