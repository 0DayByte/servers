#!/usr/bin/env bash

###############################################################
#
#	Rocket.Chat Install
#
###############################################################
###############################################################
#
#   VARIABLES
#hostname='chat'
#domain='example.com'
#packages=deb
#distro=ubuntu
#distro_version=18.04
#code_name=bionic
#
#
###############################################################
###############################################################

# Install Preqets for Rocket.Chat
apt-get update -qq && apt-get install -qqy build-essential curl software-properties-common graphicsmagick git rsync

# Node.js
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt-get -qq update
apt-get install -qqy nodejs
npm install -g inherits n && n 12.21.0

# MongoDB
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [arch=amd64] https://repo.mongodb.org/apt/${distro} ${code_name}/mongodb-org/4.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.0.list
apt-get -qq update
apt-get install -qqy mongodb-org

sed -i "s/^#  engine:/  engine: mmapv1/"  /etc/mongod.conf
sed -i "s/^#replication:/replication:\n  replSetName: rs01/" /etc/mongod.conf
systemctl enable mongod &&  systemctl start mongod
mongo --eval "printjson(rs.initiate())"


# Rocket.Chat User
useradd -m rocketchat && usermod -L rocketchat

# Install Rock.Chat
curl -L https://releases.rocket.chat/latest/download -o /tmp/rocket.chat.tgz
tar -xzf /tmp/rocket.chat.tgz -C /tmp
mv /tmp/bundle /opt/Rocket.Chat
chown -R rocketchat:rocketchat /opt/Rocket.Chat

# Rocket.Chat Service
cat << EOF | tee -a /lib/systemd/system/rocketchat.service
[Unit]
Description=The Rocket.Chat server
After=network.target remote-fs.target nss-lookup.target nginx.target mongod.target
[Service]
ExecStart=/usr/local/bin/node /opt/Rocket.Chat/main.js
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=rocketchat
User=rocketchat
Environment=MONGO_URL=mongodb://localhost:27017/rocketchat?replicaSet=rs01 MONGO_OPLOG_URL=mongodb://localhost:27017/local?replicaSet=rs01 ROOT_URL=https://${hostname}.${domain}
[Install]
WantedBy=multi-user.target
EOF

systemctl enable rocketchat
#systemctl start rocketchat

# Scripts
#wget -qO

# Cronjobs

# Rocketchat Export User List
git clone https://github.com/frdmn/rocketchat-export-userlist.git

#su rocketchat
#cd /opt/Rocket.Chat/programs/server && npm install 

#EOF
