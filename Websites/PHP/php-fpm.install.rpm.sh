#!/usr/bin/env bash
set -x #bash verbose
echo 'Setting PHP FPMs Settings'

##############################################
#
#	PHP FPM
#	Red Hat/CentOS
#
#############################################

# Add Repo
rpm -Uvh http://rpms.remirepo.net/enterprise/remi-release-7.rpm

# Yum Utilities to Set PHP7.1 as default
yum -q -y install yum-utils 

# Set PHP7.1 as default
yum-config-manager --enable remi-php71

# Update
yum -y update

# Install PHP FPM
yum -q -y install php-fpm

# Configure PHP
rm -f /etc/php.ini
wget -qO /etc/php.ini https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/rpm.php.ini
rm -f /etc/php-fpm.d/www.conf
wget -qO /etc/php-fpm.d/www.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/rpm.www.conf
 
# Enable and Start
systemctl stop php-fpm
systemctl enable php-fpm
systemctl start php-fpm
 
echo 'Done with PHP FPM Settings'
#EOF