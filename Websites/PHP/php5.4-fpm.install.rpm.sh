#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	PHP FPM
#	Red Hat/CentOS
#
#############################################

# Install PHP FPM
yum -q -y install php-fpm

# Configure PHP
rm -f /etc/php.ini
wget -qO /etc/php.ini https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/rpm.php.ini
rm -f /etc/php-fpm.d/www.conf
wget -qO /etc/php-fpm.d/www.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/rpm.www.conf
 
# Enable and Start
systemctl stop php-fpm
systemctl enable php-fpm
systemctl start php-fpm

#EOF