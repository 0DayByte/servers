#!/usr/bin/env bash
set -x #bash verbose
echo 'Setting PHP FPMs Settings'

##############################################
#
#	PHP FPM
#	Debian/ubuntu
#
#############################################

#php_version=7.3

# Add Ubuntu Ondrej Repo
apt-get -q update
apt-get install software-properties-common -qqy
add-apt-repository ppa:ondrej/php -y

# Install PHP FPM
apt-get -qqy install php${php_version}-fpm php${php_version}-curl

# Configure PHP
rm -f /etc/php/${php_version}/fpm/php.ini
wget -qO /etc/php/${php_version}/fpm/php.ini https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/deb.php.ini
rm -f /etc/php/${php_version}/fpm/pool.d/www.conf
wget -qO /etc/php/${php_version}/fpm/pool.d/www.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/deb.www.conf

# Enable and Start
systemctl enable php${php_version}-fpm
systemctl restart php${php_version}-fpm

echo 'Done with PHP FPM Settings'

#EOF