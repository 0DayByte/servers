#!/usr/bin/env bash
set -x #bash verbose
echo 'Setting PHP FPMs Settings'

##############################################
#
#	PHP FPM
#	Debian/ubuntu
#
#############################################

# Pre
apt -qqy install ca-certificates apt-transport-https

# Add Debian Sury Repo
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
apt update

# Install PHP FPM 7.1 as PHP 7.2 had errors with PHPMyAdmin
apt-get -qqy install php7.1-fpm php7.1-curl

# Configure PHP
rm -f /etc/php/7.1/fpm/php.ini
wget -qO /etc/php/7.1/fpm/php.ini https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/deb.php.ini
rm -f /etc/php/7.1/fpm/pool.d/www.conf
wget -qO /etc/php/7.1/fpm/pool.d/www.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/deb.www.conf

# Enable and Start
systemctl stop php7.1-fpm
systemctl enable php7.1-fpm
systemctl start php7.1-fpm

echo 'Done with PHP FPM Settings'
#EOF
