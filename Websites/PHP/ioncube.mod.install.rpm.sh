#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	IonCube PHP Mod Install
#   CentOS 7
#
#############################################

#php_version='5.6'

# Dowload
cd /tmp
wget http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
tar xfz ioncube_loaders_lin_x86-64.tar.gz

# Install
cd ioncube
cp ioncube_loader_lin_$php_version.so /usr/lib64/php/modules
echo "zend_extension = /usr/lib64/php/modules/ioncube_loader_lin_$php_version.so" >> /etc/php.ini

# Clean UP
cd /tmp
rm -rf ioncube*

#EOF