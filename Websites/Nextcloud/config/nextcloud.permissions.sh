##!/usr/bin/env bash
#set -x #bash verbose

###########################################################
#
# Set Nextcloud Permissions
# 
###########################################################
printf "Starting with Setting Permissions for Nextcloud\n"

printf "Load Variables\n"
rootuser='root'
htuser='www-data'
htgroup='www-data'
nextcloud='/var/www/nextcloud'
nextcloud_data='/mnt/Files/Nextcloud'
maintenance='/var/www/maintenance/'

printf "Creating possible missing Directories\n"
mkdir -p ${nextcloud}/app/assets
mkdir -p ${nextcloud}/app/updater
mkdir -p ${nextcloud_data}
mkdir -p ${maintenance}
mkdir -p ${maintenance}/nextcloud
mkdir -p ${maintenance}/nextcloud/backup

printf "chown Directories\n"
chown -R ${rootuser}:${htgroup} ${nextcloud}
chown ${htuser}:${htgroup} ${nextcloud}/app/.htaccess
chown ${htuser}:${htgroup} ${nextcloud}/app/.user.ini
chown -R ${htuser}:${htgroup} ${nextcloud}/app/apps/
chown -R ${htuser}:${htgroup} ${nextcloud}/app/assets/
chown -R ${htuser}:${htgroup} ${nextcloud}/app/config/
chown -R ${htuser}:${htgroup} ${nextcloud}/app/themes/
chown -R ${htuser}:${htgroup} ${nextcloud}/app/updater/
chown -R ${htuser}:${htgroup} ${nextcloud_data}
chown -R ${rootuser}:${rootuser} ${maintenance}

printf "chmod Files and Directories\n"
find ${nextcloud}/ -type f -print0 | xargs -0 chmod 2640
find ${nextcloud}/ -type d -print0 | xargs -0 chmod 2750
find ${nextcloud_data}/ -type f -print0 | xargs -0 chmod 2660
find ${nextcloud_data}/ -type d -print0 | xargs -0 chmod 2770
find ${maintenance}/ -type f -print0 | xargs -0 chmod 2600
find ${maintenance}/ -type d -print0 | xargs -0 chmod 2700
chmod +x ${nextcloud}/app/occ
chmod +x ${maintenance}/nextcloud/*.sh

printf "Finished with Setting Permissions for Nextcloud\n"

#EOF