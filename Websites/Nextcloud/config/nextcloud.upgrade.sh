##!/usr/bin/env bash
set -x #bash verbose

###########################################################
#
# Upgrading Nextcloud
# 
###########################################################

printf "Load Variables\n"
nextcloud_version='13.0.2'
rootuser='root'
htuser='www-data'
htgroup='www-data'
nextcloud='/var/www/nextcloud'
nextcloud_data='/mnt/Files/Nextcloud'
maintenance='/var/www/maintenance/'

printf "Starting with Upgrade for Nextcloud ${nextcloud_version}\n"
. ${maintenance}/nextcloud/nextcloud.backup.sh
sleep 10


printf "Prep\n"
cd ${nextcloud}/app
sudo -u www-data php occ maintenance:mode --on
systemctl stop nginx

printf "Nextcloud Upgrade to ${nextcloud_version}\n"
cd ${nextcloud}
wget -q https://download.nextcloud.com/server/releases/nextcloud-${nextcloud_version}.zip
unzip nextcloud-${nextcloud_version}.zip
cd ${nextcloud}/app/apps/
rm -rf activity
rm -rf admin_audit
rm -rf comments
rm -rf dav
rm -rf encryption
rm -rf federatedfilesharing
rm -rf federation
rm -rf files
rm -rf files_external
rm -rf files_pdfviewer
rm -rf files_sharing
rm -rf files_texteditor
rm -rf files_trashbin
rm -rf files_versions
rm -rf files_videoplayer
rm -rf firstrunwizard
rm -rf gallery
rm -rf logreader
rm -rf lookup_server_connector
rm -rf nextcloud_announcements
rm -rf notifications
rm -rf oauth2
rm -rf password_policy
rm -rf provisioning_api
rm -rf serverinfo
rm -rf sharebymail
rm -rf survey_client
rm -rf systemtags
rm -rf theming
rm -rf twofactor_backupcodes
rm -rf updatenotification
rm -rf user_external
rm -rf user_ldap
rm -rf workflowengine
cp -R * ${nextcloud}/nextcloud/apps
cp ${nextcloud}/app/config/config.php ${nextcloud}/nextcloud/config/config.php
rm -rf ${nextcloud}/nextcloud/config/data
#git clone https://github.com/mwalbeck/nextcloud-breeze-dark.git ${nextcloud}/nextcloud/themes/nextcloud-breeze-dark
cp -R ${nextcloud}/app/themes/nextcloud-breeze-dark ${nextcloud}/nextcloud/themes/nextcloud-breeze-dark
cd ${nextcloud}/nextcloud/themes/nextcloud-breeze-dark
git pull
cd ${nextcloud}
rm -rf app
mv nextcloud app
cd ${nextcloud}/app
sudo -u www-data php occ upgrade
sed -i -e "s/theme' => ''/theme' => 'nextcloud-breeze-dark'/g" ${nextcloud}/app/config/config.php
sed -i -e "s/511/3000/g" ${nextcloud}/app/.htaccess
sed -i -e "s/512/3010/g" ${nextcloud}/app/.htaccess
sed -i -e "s/511/3000/g" ${nextcloud}/app/.user.ini
sed -i -e "s/512/3010/g" ${nextcloud}/app/.user.ini

. ${maintenance}/nextcloud/nextcloud.permissions.sh

printf "Post\n"
#sudo -u www-data php occ maintenance:mode --off # Maintenance mode is turned off by occ upgrade
systemctl start nginx

printf "Finished with Upgrade for Nextcloud ${nextcloud_version}\n"

#EOF