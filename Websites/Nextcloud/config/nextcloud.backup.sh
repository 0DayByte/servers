##!/usr/bin/env bash
#set -x #bash verbose

###########################################################
#
# Nextcloud Backup
# 
###########################################################
printf "Starting with Backuping Up of Nextcloud\n"

printf "Load Variables\n"
now=`date +"%m-%d-%Y"`
rootuser='root'
htuser='www-data'
htgroup='www-data'
nextcloud='/var/www/nextcloud'
nextcloud_data='/mnt/Files/Nextcloud'
nextcloud_database='nextcloud'
maintenance='/var/www/maintenance/'

printf "Prep\n"
cd ${nextcloud}/app
sudo -u www-data php occ maintenance:mode --on
systemctl stop nginx

printf "Creating possible missing Directories\n"
mkdir -p ${maintenance}
mkdir -p ${maintenance}/nextcloud
mkdir -p ${maintenance}/nextcloud/backup
mkdir -p ${maintenance}/nextcloud/backup/nextcloud
mkdir -p ${maintenance}/nextcloud/backup/nextcloud/database

printf "Dumping Database\n"
mysqldump ${nextcloud_database} > ${maintenance}/nextcloud/backup/nextcloud/database/nextcloud.sql


printf "Copy Site\n"
cp -R ${nextcloud}/app ${maintenance}/nextcloud/backup/nextcloud

printf "Backing Up\n"
cd ${maintenance}/nextcloud/backup/
zip -r nextcloud-${now}.zip nextcloud
rm -rf ${maintenance}/nextcloud/backup/nextcloud

printf "Post\n"
cd ${nextcloud}/app
sudo -u www-data php occ maintenance:mode --off
systemctl start nginx

printf "Finished with Backuping Up of Nextcloud\n"

#EOF