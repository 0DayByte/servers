#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#   Elastic Search Server
#   for Nextcloud
#
###############################################################
printf "Starting with Installing Elastic Search for Nextcloud\n"

# Install Elastic Server (ref install script here)

sleep 5
sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-attachment
sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-phonetic
systemctl restart elasticsearch

#EOF
