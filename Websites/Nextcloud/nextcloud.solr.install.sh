#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#   Nextcloud
#
###############################################################

path='/var/www/nextcloud'

cd ${path}
sudo -u www-data ./occ nextant:index

#EOF