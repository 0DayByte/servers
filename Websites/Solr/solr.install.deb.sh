#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	Solr
#	Debian/Ubuntu
#
###############################################

solr_version='7.2.0'

# Install Requirements
apt-get -qqy install openjdk-8-jdk-headless

# Download
cd /opt
wget -q http://apache.org/dist/lucene/solr/${solr_version}/solr-${solr_version}.tgz
tar xzf solr-${solr_version}.tgz solr-${solr_version}/bin/install_solr_service.sh --strip-components=2
bash ./install_solr_service.sh solr-${solr_version}.tgz
rm /opt/install_solr_service.sh
echo 'SOLR_HEAP="1000m"' >> /etc/default/solr.in.sh
echo 'SOLR_JAVA_MEM="-Xms1000m -Xmx1000m"' >> /etc/default/solr.in.sh
mkdir /opt/solr-${solr_version}/server/logs
chown solr:solr /opt/solr-${solr_version}/server/logs

# Enable and Start
#systemctl stop solr
#systemctl enable solr
#systemctl start solr

#EOF
