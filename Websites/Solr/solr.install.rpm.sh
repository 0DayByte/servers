#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	Solr
#	Red Hat/Centos
#
###############################################

#solr_version='7.1.0'

# Install Requirements
yum -q -y install java-1.8.0-openjdk

# Download
cd /opt
wget -q http://apache.org/dist/lucene/solr/${solr_version}/solr-${solr_version}.tgz
tar xzf solr-${solr_version}.tgz
cp /opt/solr-${solr_version}/bin/install_solr_service.sh /opt/
rm -rf solr-${solr_version}
. /opt/install_solr_service.sh solr-${solr_version}.tgz

# Configure
chkconfig --add solr

# Enable and Start
#systemctl stop solr
systemctl enable solr
#systemctl start solr

#EOF