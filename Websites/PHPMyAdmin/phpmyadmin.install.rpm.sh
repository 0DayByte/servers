#!/usr/bin/env bash
set -x #bash verbose

##############################################################
#
#	PHPMyAdmin
#	Red Hat/CentOS
#
# During Install:
#	1. Don't set root password for MySQL.
#	2. Set phpmyadmin password.
#	3. Run after install mysql_secure_installation.
#	4. Don't Set Password Verification but set root password.
#
##############################################################

# Non Interactive Settings PHPMyAdmin
yum -q -y --enablerepo=remi install phpMyAdmin

# Enable phpMyAdmin with php fpm and ningx
ln -s /usr/share/phpMyAdmin /usr/share/nginx/phpmyadmin
chown nginx:nginx /usr/share/nginx/phpmyadmin
chown nginx:nginx /usr/share/phpMyAdmin
chown -h nginx:nginx /usr/share/nginx/phpmyadmin

# Nginx Configs
cat >/etc/nginx/sites-enabled/phpmyadmin <<EOL
server {
    	listen 80;
    	server_name phpmyadmin.${hosts}.${domain};
    	include /etc/nginx/snippets/ssl/redirect-ssl.conf;
    	include /etc/nginx/snippets/access.conf;	
}

server {
    	listen 443 ssl http2;
    	server_name phpmyadmin.${hosts}.${domain};
    	include /etc/nginx/snippets/access.conf;
    	include /etc/nginx/snippets/ssl/snakeoil.ssl.conf;
    	include /etc/nginx/snippets/sites/phpmyadmin.conf;
}
EOL

mkdir -p /etc/nginx/snippets/sites
wget -O /etc/nginx/snippets/sites/phpmyadmin.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/PHPMyAdmin/configs/phpmyadmin.nginx.conf
. /etc/nginx/permissions/nginx.permissions.sh

# Grant PHPMyAdmin Privileges
mysql -u root
GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
quit

# Restart Nginx
systemctl restart nginx

#EOF