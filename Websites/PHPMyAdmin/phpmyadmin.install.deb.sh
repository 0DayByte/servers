#!/usr/bin/env bash
set -x #bash verbose

##############################################################
#
#	PHPMyAdmin
#	Debian/Ubuntu
#
# During Install:
#	1. Don't set root password for MySQL.
#	2. Set phpmyadmin password.
#	3. Run after install mysql_secure_installation.
#	4. Don't Set Password Verification but set root password.
#
##############################################################

#root_db_pass='p@55w0rd'
#phpmyadmin_db_pass='p@55w0rd'

# Non Interactive PHPMyAdmin Install
debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $phpmyadmin_db_pass"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $root_db_pass"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $phpmyadmin_db_pass"
debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
DEBIAN_FRONTEND=noninteractive apt-get -qqy install phpmyadmin php-fpm php-mbstring php-gettext

# Enable phpMyAdmin with php fpm and ningx
ln -s /usr/share/phpmyadmin /var/www/phpmyadmin
chown www-data:www-data /var/www/phpmyadmin
chown www-data:www-data /usr/share/phpmyadmin
chown -h www-data:www-data /var/www/phpmyadmin

# Nginx Configs
cat >/etc/nginx/sites-enabled/phpmyadmin <<EOL
server {
    	listen 80;
    	server_name phpmyadmin.${hosts}.${domain};
    	include /etc/nginx/snippets/ssl/redirect-ssl.conf;
	    include /etc/nginx/snippets/access.conf;	
}

server {
    	listen 443 ssl http2;
    	server_name phpmyadmin.${hosts}.${domain};
    	include /etc/nginx/snippets/access.conf;
    	include /etc/nginx/snippets/ssl/snakeoil.ssl.conf;
	    include /etc/nginx/snippets/sites/phpmyadmin.conf;
}
EOL

# Nginx Config
mkdir -p /etc/nginx/snippets/sites
wget -O /etc/nginx/snippets/sites/phpmyadmin.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/PHPMyAdmin/configs/${code_name}.phpmyadmin.nginx.conf


# Grant PHPMyAdmin Privileges
mysql -u root
GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
quit;
mysql --user=root mysql
GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
quit;

# Restart Nginx
systemctl restart nginx

#EOF