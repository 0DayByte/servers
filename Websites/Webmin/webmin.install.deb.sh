#!/usr/bin/env bash
set -x #bash verbose

##############################################################
#
#   Webmin
#   Debian/Ubuntu
#   http://www.webmin.com/deb.html
#
##############################################################

# Add Repo
cat >/etc/apt/sources.list.d/webmin.list <<EOL
deb http://download.webmin.com/download/repository sarge contrib
EOL

# Add Repo Keys
cd /tmp
wget http://www.webmin.com/jcameron-key.asc
apt-key add jcameron-key.asc

# Install Requirements
apt-get -qq update
apt-get -qqy install apt-transport-https gnupg1 libwrap0 libwrap0-dev perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python libauthen-oath-perl libsocket6-perl

# Install Webmin
apt-get -qqy install webmin

# Configure
wget -qO /etc/webmin/miniserv.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Webmin/config/miniserv.conf.deb

# Fail2ban
mkdir -p /etc/fail2ban
mkdir -p /etc/fail2ban/jail.d
wget -qO /etc/fail2ban/jail.d/webmin.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Webmin/config/fail2ban.webmin.conf


# Restart
systemctl restart webmin

#EOF
