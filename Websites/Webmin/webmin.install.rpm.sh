#!/usr/bin/env bash
set -x #bash verbose

##############################################################
#
# 			         Webmin
#		         Red Hat/CentOS
#       http://www.webmin.com/rpm.html
#
##############################################################

# Add Repo
cat >/etc/yum.repos.d/webmin.repo <<EOL
[Webmin]
name=Webmin Distribution Neutral
#baseurl=http://download.webmin.com/download/yum
mirrorlist=http://download.webmin.com/download/yum/mirrorlist
enabled=1
EOL

# Add Repo Keys
cd /tmp
wget http://www.webmin.com/jcameron-key.asc
rpm --import jcameron-key.asc

# Install Requirements
yum -q -y install perl-Net-SSLeay
cd /tmp
wget ftp://ftp.pbone.net/mirror/ftp5.gwdg.de/pub/opensuse/repositories/home:/Kenzy:/packages/CentOS_7/x86_64/perl-Authen-Libwrap-0.22-12.1.x86_64.rpm
rpm -ivh perl-Authen-Libwrap-0.22-12.1.x86_64.rpm

# Install Webmin
yum -q -y install webmin

# Configure

# Restart
service webmin restart

#EOF