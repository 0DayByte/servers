#!/usr/bin/env bash
##############################################
#
# WEBMIN
#
###############################################

cd $DIR
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/Webmin/webmin.install.${packages}.sh
. webmin.install.${packages}.sh