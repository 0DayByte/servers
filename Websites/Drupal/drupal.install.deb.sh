#!/usr/bin/env bash
###########################################################
#
#       Drupal Install
# 
###########################################################

#php_version='7.3'
#d7_version='7.68'
#d8_version='8.8.0'
#d9_version='9.0.x-dev'

# Install Requirements
apt-get install php-memcached php${php_version}-ssh2 php${php_version}-apcu php${php_version}-fpm php${php_version}-cli php${php_version}-intl php${php_version}-mysql php${php_version}-curl php${php_version}-gd php${php_version}-soap php${php_version}-xml php${php_version}-zip php${php_version}-mbstring php${php_version}-pdo-pgsql git curl -qqy

# Install Composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Make Directories
mkdir -p /var/www/drupal
mkdir -p /var/www/drupal/maintenance

# Download Drupal
cd /var/www/drupal
#wget https://ftp.drupal.org/files/projects/drupal-${d7_version}.tar.gz
wget https://ftp.drupal.org/files/projects/drupal-${d8_version}.tar.gz
#wget https://ftp.drupal.org/files/projects/drupal-${d9_version}.tar.gz
#tar xvfz drupal-${d7_version}.tar.gz
tar xvfz drupal-${d8_version}.tar.gz
#tar xvfz drupal-${d9_version}.tar.gz
#rm drupal-${d7_version}.tar.gz
rm drupal-${d8_version}.tar.gz
#rm drupal-${d9_version}.tar.gz

# PHP Config
systemctl stop php${php_version}-fpm
wget -qO /etc/php/${php_version}/fpm/pool.d/www.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/deb.www.${php_version}.conf
wget -qO /etc/php/${php_version}/fpm/php.ini https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/deb.php.${php_version}.ini
systemctl start php${php_version}-fpm

# MariaDB Config
systemctl stop mysql
wget -qO /etc/php/${php_version}/fpm/pool.d/www.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/deb.www.${php_version}.conf
wget -qO /etc/php/${php_version}/fpm/php.ini https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/configs/deb.php.${php_version}.ini
systemctl start mysql

#EOF