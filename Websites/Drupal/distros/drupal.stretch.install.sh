#!/usr/bin/env bash
set -x #bash verbose
echo "Starting with Drupal Install"
echo "    on Debian Stretch"

############################################################
#
#	Drupal Install on Debian Stretch
#	MariaDB,Drupal,PHP-FPM,Webmin,Postfix,Memcache
#
############################################################

packages=deb
mariadb_version=10.3
distro=debian
code_name=stretch

# Make Install Directory
cd /root
mkdir install
cd install

# Set Source Directory of Bash scripts
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# -Set permissions for scripts
chown -R root:root $DIR
chmod -R 700 $DIR

# Update
apt -q update
apt -qqy dist-upgrade
apt -qqy install locales ca-certificates
dpkg-reconfigure locales
dpkg-reconfigure tzdata

################# Install

# System Configs
mkdir system
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.conf.sh
. sysctl.conf.sh

# Apt
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/repos.${distro}.sh
. repos.${distro}.sh

# Common Packages
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/Common/common.packages.deb.sh
. common.packages.deb.sh

# OpenSSH
mkdir openssh 
cd openssh
wget https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/openssh.install.sh
. openssh.install.sh

# Webmin
cd $DIR
mkdir webmin
cd webmin
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/Webmin/webmin.install.sh
. webmin.install.sh

# Drupal
cd $DIR
mkdir drupal
cd drupal
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/Drupal/drupal.install.sh

# MariaDB
cd $DIR/drupal
wget https://gitlab.com/0DayByte/servers/raw/master/Databases/MariaDB/mariadb.install.sh
. mariadb.install.sh

# Memcache
cd $DIR/drupal
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/memcached.install.sh
. memcached.install.sh

# SSL
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/openssl.install.sh
. openssl.install.sh

# Nginx
cd $DIR/drupal
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/nginx.install.sh
. nginx.install.sh

# Postfix
cd $DIR/drupal
wget https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/postfix.install.sh
. postfix.install.sh

# Iptables
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/IPTables/iptables.install.sh
. iptables.install.sh

# Fail2Ban
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/Fail2Ban/fail2ban.install.sh
. fail2ban.install.sh

# DNSMasq
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/DNSMasq/dnsmasq.install.sh
. dnsmasq.install.sh

echo "Finished with Drupal Install"
echo "    on Debian Stretch"