#!/usr/bin/env bash
###########################################################
#
# Drupal Install
# 
###########################################################


#d8_version='8.3.7'
#d7_version='7.56'

#mariadb_version=10.3
#distro=debian
#distro_name=stretch

#packages=deb

# MariaDB
#cd $DIR
#mkdir mariadb 
#cd mariadb
#wget https://gitlab.com/0DayByte/servers/raw/master/Databases/MariaDB/mariadb.install.sh
#. mariadb.install.sh






# Install NGINX
nginx

# Install MariaDB
mysql-server 

# Install PHP FPM
php-fpm 

# Install Memcache
memcache

# 
apt-get install -qqy php-ssh2 php-apcu php-fpm php-cli php-mcrypt php-intl php-mysql php-curl php-gd php-soap php-xml php-zip php-mbstring


# Download
mkdir -p /var/www/drupal
mkdir -p /var/www/drupal/maintenance
cd /var/www/drupal
wget https://ftp.drupal.org/files/projects/drupal-$d7_version.tar.gz
wget https://ftp.drupal.org/files/projects/drupal-$d8_version.tar.gz
tar xvfz drupal-$d7_version.tar.gz
tar xvfz drupal-$d8_version.tar.gz
rm drupal-$d7_version.tar.gz
rm drupal-$d8_version.tar.gz

#EOF