#!/usr/bin/env bash
#set -x #bash verbose

###############################################################
#
#   Elastic Search Install
#
###############################################################
printf "Starting with Elastic Search Install\n"

# install java (openjdk-8-jre-headless script here)

cd
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
apt-get -qq update && apt-get -qqy install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
apt-get -qq update && apt-get -qqy install elasticsearch
printf "Finished with Elastic Search Install\n"

#EOF