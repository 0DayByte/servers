#!/usr/bin/env bash
set -x #bash verbose

################################################
#
#		Set OpenSSL Permissions
#
################################################

echo 'Starting Setting SSL Permissions'

chown -R root:root /etc/ssl
chown -R root:ssl-cert /etc/ssl/private
find /etc/ssl -type f -print0 | xargs -0 chmod 2644
find /etc/ssl -type d -print0 | xargs -0 chmod 2755
find /etc/ssl/private -type f -print0 | xargs -0 chmod 2640
find /etc/ssl/private -type d -print0 | xargs -0 chmod 2750
chmod 2600 /etc/ssl/ssl.permissions.sh
chmod +x /etc/ssl/ssl.permissions.sh

echo 'Finished Setting SSL Permissions'

#EOF