#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
#		OpenSSL
#		Debian/Ubuntu
#
#########################################################

# Install
apt-get install openssl ca-certificates -qqy

# Create dhparam
#openssl dhparam -out /etc/ssl/private/dhparams.2048.pem 2048
#openssl dhparam -out /etc/ssl/private/dhparams.4096.pem 4096

# Get dhparam
wget -qO /etc/ssl/private/dhparams.2048.pem https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/dhparams.2048.pem
wget -qO /etc/ssl/private/dhparams.4096.pem https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/dhparams.4096.pem

# Set SSL Permissions
wget -O /etc/ssl/ssl.permissions.sh https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/ssl.permissions.sh
. /etc/ssl/ssl.permissions.sh

#EOF
