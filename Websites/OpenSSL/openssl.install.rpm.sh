#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
#		OpenSSL
#		Red Hat/CentOS
#
#########################################################

# Install
yum -q -y install openssl ca-certificates

# Create dhparam
openssl dhparam -out /etc/ssl/private/dhparams.2048.pem 2048
openssl dhparam -out /etc/ssl/private/dhparams.4096.pem 4096


# Set SSL Permissions
wget -O /etc/ssl/certs/ssl-cert-snakeoil.pem https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/ssl-cert-snakeoil.pem
mkdir -p /etc/ssl/private
wget -O /etc/ssl/private/ssl-cert-snakeoil.key https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/ssl-cert-snakeoil.key
wget -O /etc/ssl/ssl.permissions.sh https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/ssl.permissions.sh
. /etc/ssl/ssl.permissions.sh

#EOF