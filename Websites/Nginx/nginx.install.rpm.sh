#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
#	Nginx
#	Red Hat/CentOS
#
#########################################################

# Add Repo
wget -O /etc/yum.repos.d/nginx.repo https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/nginx.repo

# Install Nginx
yum -q -y install nginx nginx-module-geoip nginx-module-image-filter nginx-module-perl nginx-module-xslt nginx-mod-stream nginx-mod-mail ssl-cert

# Configure
rm -rf /etc/nginx/{nginx.config,conf.d}
wget -O /etc/nginx/nginx.config https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/nginx.conf
sed -i 's/www-data/root/g' /etc/nginx/nginx.config
mkdir -p /etc/nginx/snippets/ssl
mkdir -p /etc/nginx/sites-enabled
mkdir -p /etc/nginx/sites-available
wget -O /etc/nginx/snippets/ssl/ssl-ciphers.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/ssl-ciphers.conf
wget -O /etc/nginx/snippets/ssl/snakeoil.ssl.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/snakeoil.ssl.conf
wget -O /etc/nginx/snippets/ssl/redirect-ssl.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/redirect-ssl.conf
wget -O /etc/nginx/snippets/access.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/access.conf
wget -O /etc/nginx/snippets/geo-ip.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/geo-ip.conf
wget -O /etc/nginx/snippets/geo-ip-all.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/geo-ip-all.conf

# Set Permissions
wget -O /etc/nginx/nginx.permissions.sh https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/nginx.permissions.sh
sed -i 's/www-data/nginx/g' /etc/nginx/nginx.permissions.sh
. /etc/nginx/nginx.permissions.sh

# Restart
systemctl stop nginx
rm -f /run/nginx.pid
systemctl start nginx

#EOF