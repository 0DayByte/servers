#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
#	Setting Nginx Permissions
#
##############################################

echo "Starting with Setting NGINX Permissions"

# - Set Permissions for Nginx Configs
chown -R root:root /etc/nginx/sites-enabled /etc/nginx/sites-available /etc/nginx/snippets /etc/nginx/{nginx.conf,nginx.permissions.sh}
chmod 2644 /etc/nginx/sites-enabled/* /etc/nginx/sites-available/* /etc/nginx/nginx.conf
chmod 2600 /etc/nginx/nginx.permissions.sh
chmod +x /etc/nginx/nginx.permissions.sh
find /etc/nginx/snippets -type d -print0 | xargs -0 chmod 2755
find /etc/nginx/snippets -type f -print0 | xargs -0 chmod 2644

# - Set Permissions on Default Site
chown -R www-data:www-data /var/www/error
find /var/www/html -type f -print0 | xargs -0 chmod 2644
find /var/www/html -type d -print0 | xargs -0 chmod 2755

echo "Finished with Setting NGINX Permissions"

#EOF