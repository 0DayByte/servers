#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
#	Nginx
#	Debian/Ubuntu
#
#########################################################

#hostname='subdomain'
#domain='example.com'
#cerbot_email='webmaster@example.com'

# Add Repo
#add-apt-repository -y ppa:nginx/stable
#apt-get -q update

# Install Nginx
apt-get -qqy install nginx-extras ssl-cert python-certbot-nginx apache2-utils

# Configure
rm -f /etc/nginx/nginx.config
wget -O /etc/nginx/nginx.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/nginx.conf
mkdir -p /etc/nginx/snippets/ssl
wget -O /etc/nginx/snippets/ssl/ssl-ciphers.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/ssl-ciphers.conf
wget -O /etc/nginx/snippets/ssl/snakeoil.ssl.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/snakeoil.ssl.conf
wget -O /etc/nginx/snippets/ssl/redirect-ssl.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/redirect-ssl.conf
wget -O /etc/nginx/snippets/access.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/access.conf
wget -O /etc/nginx/snippets/geo-ip.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/geo-ip.conf
wget -O /etc/nginx/snippets/geo-ip-all.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/geo-ip-all.conf
# Set Permissions
wget -O /etc/nginx/nginx.permissions.sh https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/nginx.permissions.sh
. /etc/nginx/nginx.permissions.sh

# Fail2ban
mkdir -p /etc/fail2ban
mkdir -p /etc/fail2ban/jail.d
wget -qO /etc/fail2ban/jail.d/nginx.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/configs/fail2ban.nginx.conf

# Restart
systemctl restart nginx

# CertBot
certbot --nginx -d $hostname.$domain --non-interactive --agree-tos -m $cerbot_email

#EOF