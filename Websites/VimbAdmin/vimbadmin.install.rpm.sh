#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	VimbAdmin
#	Red Hat/Ubuntu
#	https://github.com/magenx/magenx-email-server
#	https://easyengine.io/tutorials/mail/server/postfix-dovecot-ubuntu/
#
###################################################################

# Set Install Path
#vimbadmin_db_pass='p@55w0rd'

#  Install Requirements
yum -q -y install php-pecl-memcached php-pecl-apcu php-mbstring php-mcrypt php-json php-mysql php-cli git

# Install Composer
cd /tmp
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# Create User and Group for VimbAdmin
groupadd -g 2000 vmail
useradd -c 'Virtual Mailboxes' -d /srv/vmail -g 2000 -u 2000 -m vmail
usermod -a -G vmail postfix
usermod -a -G vmail dovecot

# Install VimbAdmin from Git
git clone https://github.com/opensolutions/ViMbAdmin.git /usr/share/nginx/vimbadmin
chown -R vmail:vmail /usr/share/nginx/vimbadmin
cd /usr/share/nginx/vimbadmin
su vmail
composer install
exit

# All Ownership for Webserver
chown -R nginx:nginx /usr/share/nginx/vimbadmin/var

# Create Database and VimbAdmin Database User
mysql -u root
CREATE DATABASE `vimbadmin`;
GRANT ALL ON `vimbadmin`.* TO `vimbadmin`@`localhost` IDENTIFIED BY '${vimbadmin_db_pass}';
FLUSH PRIVILEGES;
quit

# Copy Configs
cp /usr/share/nginx/vimbadmin/public/.htaccess.dist /usr/share/nginx/vimbadmin/public/.htaccess
cp /usr/share/nginx/vimbadmin/application/configs/application.ini.dist /usr/share/nginx/vimbadmin/application/configs/application.ini
sed -i 's/server.pop3.enabled = 1/server.pop3.enabled = 0/' /usr/share/nginx/vimbadmin/application/configs/application.ini
sed -i "s/resources.doctrine2.connection.options.password = 'xxx'/resources.doctrine2.connection.options.password = '${vimbadmin_db_pass}'/" /usr/share/nginx/vimbadmin/application/configs/application.ini



sed -i "s/p@55w0rd/${vimbadmin_db_pass}/g" /usr/share/nginx/vimbadmin/application/configs/application.ini

# Configure Postfix
mkdir -p /etc/postfix/mysql
mkdir -p /etc/postfix/config
rm -f /etc/postfix/master.cf
wget -qO /etc/postfix/master.cf https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/master.cf
rm -f /etc/postfix/main.cf
wget -qO /etc/postfix/main.cf https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/main.cf
wget -qO /etc/postfix/smtp_reply_filter https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/smtp_reply_filter
wget -qO /etc/postfix/config/black_client https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/black_client
wget -qO /etc/postfix/config/black_client_ip https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/black_client_ip
wget -qO /etc/postfix/config/block_dsl https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/block_dsl
wget -qO /etc/postfix/config/helo_checks https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/helo_checks
wget -qO /etc/postfix/config/mx_access https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/mx_access
wget -qO /etc/postfix/config/white_client https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/white_client
wget -qO /etc/postfix/config/white_client_ip https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/white_client_ip
wget -qO /etc/postfix/mysql/virtual-alias-maps.cf https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/virtual-alias-maps.cf
wget -qO /etc/postfix/mysql/virtual-mailbox-domains.cf https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/virtual-mailbox-domains.cf
wget -qO /etc/postfix/mysql/virtual-mailbox-maps.cf https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/virtual-mailbox-maps.cf
sed -i 's/p@55w0rd/${vimbadmin_db_pass}/g' /etc/postfix/mysql/virtual-alias-maps.cf
sed -i 's/p@55w0rd/${vimbadmin_db_pass}/g' /etc/postfix/mysql/virtual-mailbox-domains.cf
sed -i 's/p@55w0rd/${vimbadmin_db_pass}/g' /etc/postfix/mysql/virtual-mailbox-maps.cf


# Configure Dovecot


# Cinfigure Nginx
wget -qO /etc/nginx/sites-enabled/vimbadmin https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/vimbadmin.nginx.conf
wget -qO /etc/nginx/snippets/sites/vimbadmin.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/VimbAdmin/configs/vimbadmin.nginx.site.conf












# Generate Database Tables for VimbAdmin
cd /usr/share/nginx/vimbadmin
./bin/doctrine2-cli.php orm:schema-tool:create


#EOF