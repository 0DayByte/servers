#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
#	Let's Encrypt
#	Debian/Ubuntu
#
#########################################################

# Install Requirements
apt-get -qqy install software-properties-common

# Install Repo
add-apt-repository ppa:certbot/certbot -y
apt-get update

# Nginx
#apt-get -qqy install python-certbot-nginx

# Apachee
#apt-get -qqy install python-certbot-apache

# Install
apt -qqy install certbot

#EOF