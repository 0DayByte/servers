#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
#	Let's Encrypt
#	Red Hat/CentOS
#
#########################################################

# Install
yum -q -y install certbot

# Nginx
#yum -q -y install certbot-nginx

# Apachee
#yum -q -y install certbot-apache

#EOF