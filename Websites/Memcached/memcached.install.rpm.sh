#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	Memcached
#	Red Hat/CentOS
#
###################################################################

# Install Memcached
yum -q -y install memcached

# Configs
wget -qO /etc/sysconfig/memcached https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/configs/rpm.memcached.conf

# Enable and Restart
systemctl stop memcached
systemctl enable memcached
systemctl enable memcached

#EOF