#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	Memcached
#	Debian/Ubuntu
#
###################################################################

# Install Memcached
apt-get -qqy install memcached

# Configs
rm -f /etc/memcached/memcached.conf
wget -qO /etc/memcached.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/configs/memcached.conf
wget -qO /etc/memcached2.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/configs/memcached2.conf
wget -qO /etc/memcached3.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/configs/memcached3.conf
wget -qO /etc/memcached4.conf https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/configs/memcached4.conf

# Systemd Services
wget -qO /etc/systemd/system/memcached2.service https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/configs/memcached2.service
wget -qO /etc/systemd/system/memcached3.service https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/configs/memcached3.service
wget -qO /etc/systemd/system/memcached4.service https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/configs/memcached4.service

# Systemd Reload Daemons
systemctl daemon-reload

# Restart/Start Sercives
systemctl stop memcached
systemctl enable memcached2
systemctl enable memcached3
systemctl enable memcached4
systemctl start memcached memcached2 memcached3 memcached4

#EOF