#!/usr/bin/env bash
###########################################################
#
#       LXDUI
#       Debian Base
#       https://github.com/AdaptiveScale/lxdui
#
###########################################################
###########################################################
#
# VARIABLES
lxdui_version='2.1.2'
python3_version='3.6'
#
###########################################################

echo 'installing lxdui on debain base system'

# Install Prerequisites
# https://github.com/AdaptiveScale/lxdui/wiki/Installing-the-Prerequisites
apt-get install -qqy git build-essential libssl-dev python3-venv python3-pip python3-dev zfsutils-linux bridge-utils

# Create User
useradd -m -d /opt/lxdui -r lxdui
usermod -a -G staff lxdui
su lxdui

# Install LXDUI
# https://github.com/AdaptiveScale/lxdui
cd /opt/lxdui && git clone https://github.com/AdaptiveScale/lxdui.git app
cd app && python3 setup.py install
exit
chown -R lxdui:lxdui app
chown -R lxdui /usr/local/lib/python${python3_version}/dist-packages/LXDUI-${lxdui_version}-py${python3_version}.egg
chown -R lxdui /usr/lib/python${python3_version}/logging

# SystemD Service
wget -O systemd/system/lxdui.service https://gitlab.com/0DayByte/servers/raw/master/Websites/LXDUI/lxdui.service
systemctl reload daemons
systemctl enable lxdui.service
systemctl start lxdui.service

echo 'access lxdui from http://127.0.0.1:15151'
echo 'finished installing lxdui on debain base system'
#EOF