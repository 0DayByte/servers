#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		FileBrowser Install
#       Debian / Ubuntu
#
###############################################################

#filebrowser_version='2.0.16'

# Create User
useradd -s /bin/false -c "FileBrowser" -g media -u 1100 -r -m filebrowser
mkdir -p /home/filebrowser/files
chown filebrowser:media /home/filebrowser/files
chmod 775 /home/filebrowser/files
#ln -s /srv/nfs/Media/Movies /home/filebrowser/files
#ln -s /srv/nfs/Media/TV /home/filebrowser/files
#ln -s /srv/nfs/Media/Archive/Movies /home/filebrowser/files
#ln -s /srv/nfs/Media/Archive/TV /home/filebrowser/files

# Install
mkdir -p $DIR/filebrowser
cd $DIR/filebrowser
wget -q https://github.com/filebrowser/filebrowser/releases/download/v$filebrowser_version/linux-amd64-filebrowser.tar.gz
tar -zxvf linux-amd64-filebrowser.tar.gz
mv filebrowser /usr/local/bin/
chmod +x /usr/local/bin/filebrowser

# Start
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/FileBrowser/filebrowser.service -O /etc/systemd/system/filebrowser.service
systemctl daemon-reload
systemctl start filebrowser
systemctl enable filebrowser

# Nginx Reverse Proxy
wget https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/nginx.proxy -O /etc/nginx/snippets/proxy.conf
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/FileBrowser/filebrowser.nginx -O /etc/nginx/sites-available/filebrowser
cp /etc/nginx/sites-available/filebrowser /etc/nginx/sites-enabled
systemctl reload nginx

#EOF