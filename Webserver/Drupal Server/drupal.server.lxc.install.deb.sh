#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#	Web LXC Install on Ubuntu Bionic (run time 7 mins)
#	MariaDB,Drupal,PHP-FPM,Webmin,Postfix,Memcache,DNSMasq
#
###############################################################
###############################################################
#
#   VARIABLES
#hostname='drupal'
#domain='example.com'
#packages=deb
#distro=ubuntu
#distro_version=18.04
#code_name=bionic
#php_version=7.3
#ip4_address='192.168.1.5/24'
#ip4_gateway='192.168.1.1'
#ip6_address='2002:6n90:40::4/48'
#ip6_gateway='2002:6n90:40::1'
#ip4_dns1='192.168.1.1'
#ip4_dns2='1.1.1.1'
#ip6_dns1=2345:6n90:40::1'
#ip6_dns2='2606:4700:4700::1111'
#netplan_file='50-cloud-init'
#mariadb_version=10.4
#mariadb_root_pass=P@55w0rd
#
###############################################################
###############################################################

echo "starting with ${hostname} install on ${distro} ${distro_version}"

# Make Install Directory
cd /usr/src/
mkdir -p ${hostname}-install
cd ${hostname}-install

# Set Source Directory of Bash scripts
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# -Set permissions for scripts
chown -R root:root /usr/src/${hostname}-install
chmod -R 700 /usr/src/${hostname}-install

# Disable AppArmor
systemctl stop apparmor
systemctl disable apparmor

# Update
apt-get -q update
apt-get -qqy install locales
echo en_US UTF-8 >> /etc/locale.gen
locale-gen
apt-get -qqy dist-upgrade
apt-get -qqy autoremove
apt-get -q autoclean
apt-get -q clean
apt-get -qqy install ca-certificates
dpkg-reconfigure tzdata

############################# Install ########################

# OpenSSH
mkdir -p $DIR/system
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/openssh.install.sh
. openssh.install.sh

# Iptables
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Networking/IPTables/iptables.install.sh
. iptables.install.sh
wget -qO /etc/iptables/rules.v4 https://gitlab.com/0DayByte/servers/raw/master/Webserver/Drupal%20Server/config/iptables/rules.v6 
wget -qO /etc/iptables/rules.v6 https://gitlab.com/0DayByte/servers/raw/master/Webserver/Drupal%20Server/config/iptables/rules.v6
iptables-restore < /etc/iptables/rules.v4
ip6tables-restore < /etc/iptables/rules.v6

# Fail2Ban
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Networking/Fail2Ban/fail2ban.install.sh
. fail2ban.install.sh

# SSL
mkdir -p $DIR/webserver
cd $DIR/drupal
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/openssl.install.sh
. openssl.install.sh

# Nginx
cd $DIR/webserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/nginx.install.sh
. nginx.install.sh

# Postfix
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/postfix.install.sh
. postfix.install.sh

# PHP
cd $DIR/webserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/PHP/php-fpm.install.ubuntu.sh
. php-fpm.install.ubuntu.sh

# MariaDB
mkdir -p $DIR/database
cd $DIR/database
wget -q https://gitlab.com/0DayByte/servers/raw/master/Databases/MariaDB/mariadb.install.sh
. mariadb.install.sh

# Drupal
mkdir -p $DIR/drupal
cd $DIR/drupal
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/Drupal/drupal.install.sh
. drupal.install.sh

# Memcache
cd $DIR/webserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/Memcached/memcached.install.sh
. memcached.install.sh

echo "starting with ${hostname} install on ${distro} ${distro_version}"

#EOF