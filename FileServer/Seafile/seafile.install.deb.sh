#!/usr/bin/env bash
set -x #bash verbose
# https://computingforgeeks.com/how-to-install-seafile-server-on-ubuntu-linux/

##########################################
#
#   SEAFILE INSTALL
#	with Nginx/MariaDB
#		   on
#   Debian Buster / Ubuntu Boinic
#   for Bulleye/Focal use libpython3.8
#
#
##########################################

#############################
#
#	VARIABLES
#
#############################
seafile_domain="sub.domain.com" # if domain is example.com
root_db_pass="p@55wr0d"
seafile_db_user="someone"
seafile_db_user_pass="p@55wr0d"
seafile_version="7.1.5"

echo "Installing Seafile on Debian/Ubuntu"

# Install Nginx

# Install MariaDB

# Create Databases
create database `seafile_ccnet` character set = 'utf8';
create database `seafile_server` character set = 'utf8';
create database `seafile_seahub` character set = 'utf8';
create user '{seafile_db_user}'@'localhost' identified by '{seafile_db_user_pass}';
GRANT ALL PRIVILEGES ON `seafile_ccnet`.* to `seafile`@localhost;
GRANT ALL PRIVILEGES ON `seafile_server`.* to `seafile`@localhost;
GRANT ALL PRIVILEGES ON `seafile_seahub`.* to `seafile`@localhost;

#############################
#
#	INSTALL REQUIREMENTS
#
#############################
apt-get update
apt-get install -qqy python3 python3-{pip,pil,ldap,urllib3,setuptools,mysqldb,memcache,requests,django-captcha} ffmpeg libpython3.6

#############################
#
#	ADD USER FOR SEAFILE
#	     TO RUN UNDER
#
#############################
groupadd -g 3000 seafile
useradd -g seafile -d /opt/seafile -m -s /bin/bash -u 8000 seafile


#############################
#
#	DOWNLOAD SEAFILE
#
#############################
cd /opt/seafile
wget https://download.seadrive.org/seafile-server_${seafile_version}_x86-64.tar.gz
tar -xzf seafile-server_${seafile_version}_x86-64.tar.gz
rm seafile-server_${seafile_version}_x86-64.tar.gz
mv seafile-server-${seafile_version} app
chown -R seafile:seafile /opt/seafile
su - seafile
cd /opt/seafile/app
. setup-seafile-mysql.sh
sleep 5
. seafile.sh start
sleep 10
. seahub.sh start

#############################
#
#	SETUP SEAFILE
#
#############################

#	Now you will be in for Seafile configuration.
#
# server name: input your seafile server name such as 'Seafile'.
# server domain name: type the domain name for your seafile server 'seafile.example.com'.
# seafile data directory: leave the configuration default and press enter.
# seafile fileserver port: leave it on the default port '8082'.

#	Now for the database configuration. You will be asked for 2 options: let the script create the database for you, or use an existing database.
#	Choose option '2' to use existing database settings.
#
# database host: default localhost
# database port: default on mysql normal port '3306'
# database user is '{seafile_db_user}' with password 'Hakase-Seafile01@'
# ccnet database: 'ccnet-db'
# seafile database: 'seafile-db'
# seahub database: 'seahub-db'

# Systemd Servcie 
cd /etc/systemd/system
wget -q https://gitlab.com/0DayByte/servers/-/raw/master/FileServer/Seafile/config/seafile.service 
wget -q https://gitlab.com/0DayByte/servers/-/raw/master/FileServer/Seafile/config/seahub.service

# Nginx



echo "Finished with Installing Seafile"