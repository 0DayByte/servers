#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		Samba Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

#samba_workgroup='WORKGROUP'

# Install
apt install samba -qqy
systemctl stop nmbd

# Config
mkdir -p /srv/samba
wget -qO /etc/samba/smb.conf https://gitlab.com/0DayByte/servers/raw/master/FileServer/Samba/config/smb.conf
sed -i 's/WORKGOUP/$samba_workgroup/' /etc/samba/smb.conf

# Fail2ban
mkdir -p /etc/fail2ban
mkdir -p /etc/fail2ban/jail.d
wget -qO /etc/fail2ban/jail.d/samba.conf https://gitlab.com/0DayByte/servers/raw/master/FileServer/Samba/config/fail2ban.samba.conf

# Restart
systemctl start nmbd

#EOF