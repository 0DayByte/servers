apt-get install -qqy php7.4-{apcu,bcmath,bz2,curl,gd,gmp,imagick,imap,intl,json,mbstring,memcached,smbclient,xml,zip} libxml2 ffmpeg

# Mysql
apt-get install -qqy php7.4-mysql

# SQLite
#apt-get install -qqy php7.4-sqlite3

# PostgreSQL
#apt-get install -qqy php7.4-pgsql