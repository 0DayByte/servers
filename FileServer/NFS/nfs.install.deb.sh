#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
#	NFS
#	Debian/Ubuntu
#
#########################################################

# Install NFS server
apt-get install nfs-kernel-server -qqy

# Install NFS CLient
apt-get install nfs-common -qqy

#EOF