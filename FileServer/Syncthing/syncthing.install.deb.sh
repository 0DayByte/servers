##!/usr/bin/env bash
set -x #bash verbose

##########################################
#
#   SYNCTHING INSTALL
#   Debian / Ubuntu
#
##########################################

# Requirements
apt -qy install curl apt-transport-https

# Add Repo
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
echo "deb https://apt.syncthing.net/ syncthing release" > /etc/apt/sources.list.d/syncthing.list
apt-get -qq update

# Install
apt-get -qqy install syncthing

# SED
# /lib/systemd/system/syncthing@.service
# 
# -no-restart with -gui-address=0.0.0.0:8384 -no-restart
# or do NGINX proxy
#EOF