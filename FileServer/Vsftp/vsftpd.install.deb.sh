#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#       VSFTP fileserver Install
#       Debian / Ubuntu
#
###############################################################
###############################################################

# Install
apt install vsftpd -qqy
systemctl stop vsftpd

# Config
wget -qO /etc/vsftpd.conf https://gitlab.com/0DayByte/servers/raw/master/FileServer/Vsftp/config/vsftp.conf
touch /etc/vsftpd.userlist
sed -i 's/example.com/$hostname.$domain/' /etc/vsftpd.conf

# Fail2ban
echo "net.netfilter.nf_conntrack_helper=1" >> /etc/sysctl.conf
sysctl --system
touch /var/log/vsftpd.log
mkdir -p /etc/fail2ban
mkdir -p /etc/fail2ban/jail.d
wget -qO /etc/fail2ban/jail.d/vsftp.conf https://gitlab.com/0DayByte/servers/raw/master/FileServer/Vsftp/config/fail2ban.vsftp.conf


# Restart
systemctl start vsftpd

#EOF