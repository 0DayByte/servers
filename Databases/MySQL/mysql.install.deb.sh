#!/usr/bin/env bash

#########################################################
#
#       MySQL Install
#       Debian/Ubuntu
#
#########################################################

# Update Repo
apt-get -q update

# Install MySQL
apt-get -qqy install mysql-server
mysql_secure_installation

#EOF
