#######################################
#
# MySQL 5.6 Install
# Debain (8/9)
#
#######################################

apt-get -q update
apt-get -qqy dist-upgrade
apt-get -qqy install ca-certificates
echo -e "deb http://repo.mysql.com/apt/debian/ stretch mysql-5.6\ndeb-src http://repo.mysql.com/apt/debian/ stretch mysql-5.6" > /etc/apt/sources.list.d/mysql.list
wget -O /tmp/RPM-GPG-KEY-mysql https://repo.mysql.com/RPM-GPG-KEY-mysql
apt-key add /tmp/RPM-GPG-KEY-mysql
apt-get -q update
apt-get -qqy install mysql-server

# EOF