#!/usr/bin/env bash
#########################################################
#
#       MariaDB
#       Debian/Ubuntu
#
#########################################################

#mariadb_version=10.3
#mariadb_root_pass=P@55w0rd
#distro=debian
#distro_name=stretch

# Requirements
apt-get update -q
apt-get install software-properties-common -qqy
apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'

# Add Repo
cat >/etc/apt/sources.list.d/mariadb.list <<EOL
deb [arch=amd64] https://mirror.rackspace.com/mariadb/repo/${mariadb_version}/${distro} ${code_name} main
deb-src https://mirror.rackspace.com/mariadb/repo/${mariadb_version}/${distro} ${code_name} main
EOL

# Update Repo
apt-get -q update

# Install MariaDB
apt-get -qqy install mariadb-server bc
systemctl stop mysql
rm -rvf /var/lib/mysql/ib_logfile*
chown -R mysql:mysql /var/lib/mysql
wget -qO /etc/mysql/mariadb.conf.d/50-server.cnf https://gitlab.com/0DayByte/servers/raw/master/Databases/MariaDB/config/mariadb.50-server.cnf
wget -qO /etc/mysql/tuning-primer.sh https://github.com/BMDan/tuning-primer.sh/raw/master/tuning-primer.sh
systemctl start mysql
sleep 5

# Secure MySQL
mysql_secure_installation

#EOF