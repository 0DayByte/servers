#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
#	MariaDB
#	Red Hat/CentOS
#
#########################################################

# Add Repo
cat >/etc/yum.repos.d/mariadb.repo <<EOL
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.1/${distro}-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOL

# Install MariaDB
yum -q -y install MariaDB-server 

#EOF