#!/usr/bin/env bash
set -x #bash verbose

##############################################
#
# 		      X2GO
# 		Red Hat/CentOS
#
###############################################

# Install repository
yum -qy install epel-release

# Install Requirements
yum -qy install fuse-sshfs wget

# Install X2GO
yum -qy install x2goserver x2goserver-xsession x2gomatebindings x2goclient x2godesktopsharing

# Install X2GO Mate Binding
cd /tmp
wget ftp://ftp.pbone.net/mirror/ftp5.gwdg.de/pub/opensuse/repositories/X11:/RemoteDesktop:/x2go/Fedora_21/x86_64/x2gomatebindings-0.0.1.3-1.1.x86_64.rpm
rpm -ivh x2gomatebindings-0.0.1.3-1.1.x86_64.rpm

#EOF