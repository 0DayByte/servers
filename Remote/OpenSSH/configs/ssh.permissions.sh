#!/usr/bin/env bash
set -x #bash verbose

############################################
#
# 	Set SSH Permissions
#
##########################################

echo "Starting with Setting SSH Permissions"

# - Set Permissions
chown -R root:root /etc/ssh
find /etc/ssh -type d -print0 | xargs -0 chmod 2755
find /etc/ssh -type f -print0 | xargs -0 chmod 2644
find /etc/ssh -type f -name 'ssh_host_*_key' -exec chmod 2600 {} \;
chmod 2600 /etc/ssh/ssh.permissions.sh
chmod +x /etc/ssh/ssh.permissions.sh

echo "Starting with Setting SSH Permissions"

#EOF