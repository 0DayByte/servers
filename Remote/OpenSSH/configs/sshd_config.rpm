# Banner on log in
Banner /etc/ssh/banner.txt

# What ports, IPs and protocols we listen for
Port 55666

# Use these options to restrict which interfaces/protocols sshd will bind to
#ListenAddress ::
AddressFamily inet
ListenAddress 0.0.0.0

# Force protocol
Protocol 2

# Ciphers
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes256-ctr
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha1
# SSH hardening, see https://stribika.github.io/2015/01/04/secure-secure-shell.html
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256

# HostKeys for protocol version 2
HostKey /etc/ssh/ssh_host_ed25519_key
HostKey /etc/ssh/ssh_host_rsa_key

# Privilege Separation is turned on for security
UsePrivilegeSeparation yes

# Authentication:
LoginGraceTime 120
PermitRootLogin without-password
StrictModes yes
PubkeyAuthentication yes
AuthorizedKeysFile %h/.ssh/authorized_keys

# Don't read the user's ~/.rhosts and ~/.shosts files
IgnoreRhosts yes

# similar for protocol version 2
HostbasedAuthentication no

# Uncomment if you don't trust ~/.ssh/known_hosts for RhostsRSAAuthentication
IgnoreUserKnownHosts yes

# To enable empty passwords, change to yes (NOT RECOMMENDED)
PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Change to no to disable tunnelled clear text passwords
PasswordAuthentication no

# Other Authenication besides PAM
#KerberosAuthentication no
#KerberosGetAFSToken no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
GSSAPIAuthentication no

AllowTcpForwarding yes
X11Forwarding yes
X11UseLocalhost no
X11DisplayOffset 10
PrintMotd no
PrintLastLog yes

#UseLogin no

# Allow client to pass locale environment variables
AcceptEnv LANG LC_*

Subsystem	sftp    /usr/libexec/openssh/sftp-server

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM no

# Logging should be set to go to the /var/log/auth.log facility by using 
# the SysLog AUTH parameter. This will ensure that any problems around 
# invalid logins or the like are forwarded to a central security file for 
# auditing purposes. Print last login just prints last login and ip of last
# session
SyslogFacility AUTH
LogLevel ERROR
PrintLastLog yes

# Use server as a gateway to other machines.
GatewayPorts no

# Only allow these users
AllowUsers root

# To prevent users from being able to present environment options to the SSH 
# daemon and potentially bypass some access restrictions, add or correct the 
# following line:
PermitUserEnvironment no

# This might create a latency between the client and the server when trying 
# to establish the connection. You can disable it by using this setting:
UseDNS no

# Set Idle Timeout Interval. It is recommended to lower the idle timeout to 
# avoid unattended ssh session.
ClientAliveInterval 10000
ClientAliveCountMax 36
TCPKeepAlive yes

# Set maximum startup connections. Specifies the maximum number of concurrent
# unauthenticated connections to the SSH daemon. This setting can be helpful against 
# a brute-force script that performs forking.
MaxStartups 10:30:60
KeepAlive yes

# TCP Wrapper is a host-based Networking ACL system, used to filter network access to
# Internet. OpenSSH does supports TCP wrappers. Just update your /etc/hosts.allow file
# as follows to allow SSH only from 192.168.1.2 172.16.23.12:
#sshd : 192.168.1.2 172.16.23.12
