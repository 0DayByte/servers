#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#                        OpenSSH
#                    Ubuntu/Debian 
#
###############################################################

# Install
apt-get -qq update
apt-get -qqy install ssh

# Remove Configs
rm -f /etc/ssh/{ssh_config,sshd_config}

# Set Configs
wget -O /etc/ssh/ssh_config https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/configs/ssh_config
wget -O /etc/ssh/sshd_config https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/configs/sshd_config.deb

# Remove Moduli less than 2000 (must create strongs keys afterwards)
awk '$5 > 2000' /etc/ssh/moduli > "/tmp/moduli"
#wc -l "/tmp/moduli" # make sure there is something left
mv -f "/tmp/moduli" /etc/ssh/moduli

# Create Strong SSH Keys
cd /etc/ssh
rm -f *key*
ssh-keygen -t ed25519 -f ssh_host_ed25519_key -N '' < /dev/null
ssh-keygen -t rsa -b 4096 -f ssh_host_rsa_key -N '' < /dev/null

# Set Permissions
wget -O /etc/ssh/ssh.permissions.sh https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/configs/ssh.permissions.sh
. /etc/ssh/ssh.permissions.sh

# Copy over Public Keys to Server
mkdir -p /root/.ssh
cd /root/.ssh
wget https://gitlab.com/0DayByte/public-keys/raw/master/ED25519/Erik/erik.id.25519.pub
for f in *.pub; do (cat "${f}"; echo) >> authorized_keys; done
chown -R root:root /root/.ssh
chmod -R 2600 /root/.ssh

# Start
#systemctl restart sshd

#EOF