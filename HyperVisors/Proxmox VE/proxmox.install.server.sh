#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		HV05 Install on Debian (run time 7 mins)
#		PROMOX VE
#
###############################################################
###############################################################
#
#   VARIABLES
#hostname='hostname'
#domain='example.com'
#packages=deb
#distro=debian
#distro_version=10
#code_name=buster
#network_interface='enp4s0'
#fail2ban_ignore_ip='123.123.123.123 234.234.234.234'
#
###############################################################
###############################################################

echo "starting with Proxmox server install"

# Make Install Directory
mkdir -p /usr/src/install
cd /usr/src/install

# Set Source Directory of Bash scripts
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# -Set permissions for scripts
chown -R root:root $DIR
chmod -R 700 $DIR

# Update
apt-get -q update
apt-get -qqy install locales-all
echo en_US UTF-8 > /etc/locale.gen
locale-gen
apt-get -qqy dist-upgrade
apt-get -qqy autoremove
apt-get -q autoclean
apt-get -q clean
apt-get -qqy install ca-certificates
#dpkg-reconfigure tzdata

################# Install

# Network Forwarding
mkdir -p $DIR/system
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.conf.network.forwarding.deb.sh
. sysctl.conf.network.forwarding.deb.sh

# System Configs
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.ipv6.conf.sh
. sysctl.ipv6.conf.sh

# Fail2Ban
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/Fail2Ban/fail2ban.install.sh
. fail2ban.install.sh
sed -i 's/55666/22/' /etc/fail2ban/jail.conf
systemctl restart fail2ban

# OpenSSH
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/openssh.install.sh
. openssh.install.sh
sed -i 's/55666/22/' /etc/ssh/sshd_config
systemctl restart sshd

# Postfix
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/postfix.install.sh
. postfix.install.sh

# SSL
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/openssl.install.sh
. openssl.install.sh

# Proxmox
mkdir -p $DIR/proxmox
cd $DIR/proxmox
wget https://gitlab.com/0DayByte/servers/raw/master/HyperVisors/Proxmox%20VE/proxmox.install.deb.sh
. proxmox.install.deb.sh

echo "finished with Proxmox server install"

#EOF
