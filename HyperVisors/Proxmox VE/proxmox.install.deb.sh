##!/usr/bin/env bash
set -x #bash verbose

##########################################
#
#   PROXMOX VE INSTALL
#   Debian only
#
##########################################

# Proxmox brings its own firm.
# the existing firmware packages should first be uninstalled:
apt-get remove --purge firmware-bnx2x firmware-realtek firmware-linux firmware-linux-free firmware-linux-nonfree lxc lxd -qqy

# Add Repo
echo "deb http://download.proxmox.com/debian/pve buster pve-no-subscription" > /etc/apt/sources.list.d/pve-no-subscription-repo.list
wget http://download.proxmox.com/debian/proxmox-ve-release-6.x.gpg -O /etc/apt/trusted.gpg.d/proxmox-ve-release-6.x.gpg

# Reload System Configs
apt-get update
apt-get upgrade -qqy
apt-get dist-upgrade -qqy

# Install Promox
apt-get install proxmox-ve postfix fail2ban -qqy
apt-get remove os-prober linux-image-amd64 linux-image-4.9.0* -qqy
wget https://gitlab.com/0DayByte/servers/raw/master/HyperVisors/Proxmox%20VE/config/pve-enterprise.list -O /etc/apt/sources.list.d/pve-enterprise.list
wget https://gitlab.com/0DayByte/servers/raw/master/HyperVisors/Proxmox%20VE/config/proxmox.conf -O /etc/fail2ban/filter.d/proxmox.conf
wget https://gitlab.com/0DayByte/servers/raw/master/HyperVisors/Proxmox%20VE/config/jail.conf -O /etc/fail2ban/jail.conf
systemctl restart fail2ban
sed -i.bak 's/NotFound/Active/g' /usr/share/perl5/PVE/API2/Subscription.pm && systemctl restart pveproxy.service


#EOF
