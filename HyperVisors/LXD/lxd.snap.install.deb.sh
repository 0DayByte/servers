##!/usr/bin/env bash
set -x #bash verbose

##########################################
#
#   LXD SNAP INSTALL
#
##########################################

# System Configs
echo "fs.inotify.max_queued_events = 1048576" >> /etc/sysctl.conf
echo "fs.inotify.max_user_instances = 1048576" >> /etc/sysctl.conf
echo "fs.inotify.max_user_watches = 1048576" >> /etc/sysctl.conf
sysctl --system

# Reload System Configs



# Remove LXD
apt -qqy remove --purge lxd lxd-client
apt -qqy install --install-recommends linux-generic-hwe-18.04


# Install LXD
sudo snap install lxd

#EOF