##!/usr/bin/env bash
set -x #bash verbose

##########################################
#
#  LXD MACVLAN PROFILE
#
##########################################

#network_interface='eno1'
#macvlan_profile='macvlan'
#lxc_distro='ubuntu'
#lxc_distro_version='18.04'
#lxc_container_name='cg01'

# Copy Default Profile
lxc profile copy default ${macvlan_profile}

# Set MACVLAN as NIC Type
lxc profile device set ${macvlan_profile} eth0 nictype macvlan

# Set Host Interface as Parent to Profile's Interface
lxc profile device set ${macvlan_profile} eth0 parent ${network_interface}

# turn dhcp stateful to true
lxc network set lxdbr0 ipv6.dhcp.stateful true

# Start Container
lxc launch -p ${macvlan_profile} ${lxc_distro}:${lxc_distro_version} ${lxc_container_name}

#EOF