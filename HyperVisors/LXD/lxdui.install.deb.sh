##!/usr/bin/env bash
set -x #bash verbose

##########################################
#
#   LXD UI
#
##########################################


# Purge LXD and AppArmor
systemctl stop apparmor
systemctl disable apparmor
apt -qqy remove --purge lxd lxd-client

# Increase the number of open files and inode limits
echo 'fs.inotify.max_queued_events = 1048576' >> /etc/sysctl.conf
echo 'fs.inotify.max_user_instances = 1048576' >> /etc/sysctl.conf
echo 'fs.inotify.max_user_watches = 1048576' >> /etc/sysctl.conf
sysctl --system

sed -i -e 's/# End of file/* soft nofile 100000/' /etc/security/limits.conf
echo '* hard nofile 100000' >> /etc/security/limits.conf
echo '# End of file' >> /etc/security/limits.conf

# Grub for Swap
sed -i -e 's:GRUB_CMDLINE_LINUX_DEFAULT="":GRUB_CMDLINE_LINUX_DEFAULT="swapaccount=1":' /etc/default/grub
update-grub

# Install Requirements
apt -qqy install --install-recommends linux-generic-hwe-18.04-edge linux-headers-generic-hwe-18.04-edge linux-tools-virtual-hwe-18.04-edge git build-essential libssl-dev python3-venv python3-pip python3-dev bridge-utils

# Install LXD
sudo snap install lxd

# Configure LXD
sudo lxd init

# 
cd /opt
git clone https://github.com/AdaptiveScale/lxdui.git
cd lxdui
python3 setup.py install
ln -s /opt/lxdui/lxdui.service /lib/systemd/system/lxdui.service
systemctl enable lxdui.service
systemctl start lxdui.service

#EOF