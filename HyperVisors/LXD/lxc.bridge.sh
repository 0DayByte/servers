##!/usr/bin/env bash
set -x #bash verbose

#################################
#                               #
#  LXD BRIDGE PROFILE           #
#                               #
#################################

#network_interface='eno1'
#bridge_profile='bridge'
#lxc_distro='ubuntu'
#lxc_distro_version='18.04'
#lxc_container_name='cg01'

# Copy Default Profile
lxc profile copy default ${bridge_profile}

# Set Host Interface as Parent to Profile's Interface
lxc profile device set ${bridge_profile} eth0 parent ${network_interface}

# Start Container
lxc launch -p ${bridge_profile} ${lxc_distro}:${lxc_distro_version} ${lxc_container_name}

#EOF
