#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#		HV04 Install on Ubuntu (run time 7 mins)
#		LXD and LXDUI
#
###############################################################
###############################################################
#
#   VARIABLES
#hostname='lxd'
#domain='example.com'
#packages='deb'
#distro='ubuntu'
#distro_version='18.04'
#code_name='bionic'
#network_interface='eno1'
#macvlan_profile='macvlan'
#lxc_distro='ubuntu'
#lxc_distro_version='18.04'
#lxc_container_name='container_name'
#fail2ban_ignore_ip='8.8.8.8'
#
###############################################################
###############################################################

echo "starting with ${hostname} install on ${distro} ${distro_version}"

# Make Install Directory
cd /usr/src/
mkdir -p install
cd install

# Set Source Directory of Bash scripts
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# -Set permissions for scripts
chown -R root:root /usr/src/install
chmod -R 2700 /usr/src/install

# Update
apt-get -q update
apt-get -qqy install locales
echo en_US UTF-8 >> /etc/locale.gen
locale-gen
apt-get -qqy dist-upgrade
apt-get -qqy autoremove
apt-get -q autoclean
apt-get -q clean
apt-get -qqy install ca-certificates
dpkg-reconfigure tzdata

################# Install

# System Configs
mkdir system
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.ipv6.conf.sh
. sysctl.ipv6.conf.sh

# Network Forwarding
mkdir -p $DIR/system
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.conf.network.forwarding.${packages}.sh
. sysctl.conf.network.forwarding.deb.sh

# Apt
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/repos.${distro}.sh
. repos.${distro}.sh

# OpenSSH
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/openssh.install.sh
. openssh.install.sh

# Iptables
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/IPTables/iptables.install.sh
. iptables.install.sh

# Fail2Ban
cd $DIR/system
wget https://gitlab.com/0DayByte/servers/raw/master/Networking/Fail2Ban/fail2ban.install.sh
. fail2ban.install.sh

# LXDUI
cd $DIR/lxd
wget https://gitlab.com/0DayByte/servers/raw/master/HyperVisors/LXD/lxdui.install.sh
. lxdui.install.sh

# LXD MCVLAN
cd $DIR/lxd
wget https://gitlab.com/0DayByte/servers/raw/master/HyperVisors/LXD/lxc.macvlan.sh
. lxc.macvlan.sh

echo "starting with ${hostname} install on ${distro} ${distro_version}"

#EOF

