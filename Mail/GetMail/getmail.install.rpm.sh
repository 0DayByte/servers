#!/usr/bin/env bash
set -x #bash verbose

####################################
#
#   GetMail
#   Red Hat/CentOS
#
####################################

# Install Getmail
yum -q -y install getmail

#EOF