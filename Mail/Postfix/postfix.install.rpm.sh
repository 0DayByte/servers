#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	Postfix
#	Red Hat/CentOS
#
###################################################################

# Install Postfix
yum -q -y install postfix3 postfix3-cdb postfix3-mysql postfix3-pcre postfix3-perl-scripts postfix3-sqlite cyrus-sasl cyrus-sasl-lib cyrus-sasl-plain mailx

# Set Sendmail alternative postix
alternatives --set mta /usr/sbin/sendmail.postfix

# Create DHParams
cd /etc/postfix
openssl dhparam -out dh_512.pem 512
openssl dhparam -out dh_1024.pem 1024

# Configs

#EOF