#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	Postfix
#	Debian/Ubuntu
#
###################################################################


# PPA only for Xenial (need to upgrade as error in postfix -v 3.0.1)
#add-apt-repository ppa:carsten-uppenbrink-net/postfix -y
#apt-get -qq update

# Install Postfix
apt-get -qqy install postfix libsasl2-modules postfix-pcre mailutils
sleep 2
systemctl stop postfix

# Configs
wget -qO /etc/postfix/main.cf https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/configs/main.cf
wget -qO /etc/postfix/master.cf https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/configs/master.cf
sed -i "s/mx/${hostname}/g" /etc/postfix/main.cf
sed -i "s/example.com/${domain}/g" /etc/postfix/main.cf

# Fail2ban
mkdir -p /etc/fail2ban
mkdir -p /etc/fail2ban/jail.d
wget -qO /etc/fail2ban/jail.d/postfix.conf https://gitlab.com/0DayByte/servers/raw/master/Mail/Postfix/configs/fail2ban.postfix.conf

systemctl start postfix

#EOF
