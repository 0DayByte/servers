#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	Dovecot
#	Red Hat/Ubuntu
#	https://devinstechblog.com/mailserver-dovecot-configuration-part-6/
#
###################################################################

# Install Dovecot
yum -q -y install dovecot22 dovecot22-mysql dovecot22-pigeonhole clamav clamav-update amavisd-new expat curl
systemctl enable dovecot
systemctl stop dovecot

# Configure ClamAV
rm -f freshclam.conf
wget -O /etc/freshclam.conf https://gitlab.com/0DayByte/servers/raw/master/Mail/Dovecot/configs/freshclam.conf

# Config Amavis
sed -i 's/host.example.com/${hostname}.${domain}/g' /etc/amavisd/amavisd.conf
sed -i 's/example.com/${domain}/g' /etc/amavisd/amavisd.conf

# Configure Dovecot
wget -qO /etc/logrotate.d/dovecot-deliver https://gitlab.com/0DayByte/servers/raw/master/Mail/Dovecot/configs/dovecot.rotate.log
wget -qO /usr/local/bin/quota-warning.sh https://gitlab.com/0DayByte/servers/raw/master/Mail/Dovecot/configs/quota-warning.sh

# Set as Default
systemctl enable dovecot

# Configs Solr
cd /opt/solr/bin
su solr
./solr create_core -c dovecot -n dovecot
#curl http://127.0.0.1:8983/solr/data/dovecot/config -d '{"set-user-property": {"update.autoCreateFields":"false"}}'
rm -f /var/solr/data/dovecot/conf/{managed-schema.xml,solrconfig.xml}
wget -qO /var/solr/data/dovecot/conf/schema.xml https://gitlab.com/0DayByte/servers/raw/master/Mail/Dovecot/configs/schema.xml
wget -qO /var/solr/data/dovecot/conf/solrconfig.xml https://gitlab.com/0DayByte/servers/raw/master/Mail/Dovecot/configs/solrconfig.xml

# Cron Job for Solr
(crontab -l && echo "*/5 *  *  *  *     curl --silent http://127.0.0.1:8983/solr/dovecot/update?commit=true") | crontab -
(crontab -l && echo "* */24  *  *  *     curl --silent http://127.0.0.1:8983/solr/dovecot/update?optimize=true") | crontab -
curl --silent http://127.0.0.1:8983/solr/dovecot/update?optimize=true
exit

# Set Permissions
chown solr:solr /var/solr/data/dovecot/conf/{schema.xml,solrconfig.xml}
chmod 664 /var/solr/data/dovecot/conf/{schema.xml,solrconfig.xml}

# Restart Solr
systemctl restart solr

# Start Dovecote
systemctl start dovecot

#EOF