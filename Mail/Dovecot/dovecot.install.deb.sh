#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	Dovecot
#	Debian/Ubuntu
#
###################################################################

# Install Dovecot
apt-get -qqy install dovecot-core dovecot-imapd dovecot-lmtpd dovecot-mysql dovecot-sieve dovecot-managesieved/
    dovecot-ldap dovecot-lucene dovecot-metadata-plugin dovecot-pgsql dovecot-pop3d dovecot-solr/
    dovecot-sqlite

# Configs
wget -qO /etc/logrotate.d/dovecot-deliver https://gitlab.com/0DayByte/servers/raw/master/Mail/Dovecot/configs/dovecot.rotate.log

# Tips
# When moving dovecot clear all logs,keyword,uid files
# only subscriptions and maildirsize in mail user root dir
# and maildirfolder in users seperate mail folders

#EOF