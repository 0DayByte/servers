#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#   Dovecot Solr
# 
# ref. http://mor-pah.net/2016/08/15/dovecot-2-2-with-solr-6-or-5/
#   
##############################################################

sudo -u solr /opt/solr/bin/solr create -c dovecot -n dovecot
rm /var/solr/data/dovecot/conf/{managed-schema,solrconfig.xml,schema.xml}
wget -qO /var/solr/data/dovecot/conf/schema.xml https://gitlab.com/0DayByte/servers/raw/master/Mail/Dovecot/configs/schema.xml
wget -qO /var/solr/data/dovecot/conf/solrconfig.xml https://gitlab.com/0DayByte/servers/raw/master/Mail/Dovecot/configs/solrconfig.xml

# Scan Users Directory
#doveadm fts rescan -u user@example.com

# Index All
#doveadm index -A * -q

#EOF