#!/bin/sh

PERCENT=$1
USER=$2

cat << EOF | /usr/lib/dovecot/dovecot-lda -d $USER -o "plugin/quota=maildir:User quota:noenforcing"
From: "CorpCom Solutions Mail" <postmaster@corp-com.net>
Subject: Mail service quota warning

Your mailbox is now $PERCENT% full.

You should delete some messages from the server,
to bring your mail box to 60% or quota,
or bring your mail box under 3 GB.


WARNING: Do not ignore this message as if your mailbox
reaches 100% of quota, new mail will be rejected.

EOF