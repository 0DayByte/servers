#!/usr/bin/env bash
set -x #bash verbose

###############################################################
#
#        ViMbAdmin Server Install
#
###############################################################
###############################################################
#
#   VARIABLES
#hostname='mail'
#domain='example.com'
#packages='deb'
#distro='ubuntu'
#distro_version='18.04'
#code_name='bionic'
#network_interface='eth0'
#cerbot_email='admin@example.com'
#fail2ban_ignore_ip='123.123.123.123 234.234.234.234'
#mariadb_version='10.4'
#mariadb_root_pass='P@55w0rd'
#
#
###############################################################
###############################################################

echo "Starting with installing media server on ${hostname} ${distro} ${distro_version}"

# Make Install Directory
cd /usr/src/
mkdir -p install
cd install

# Set Source Directory of Bash scripts
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

# -Set permissions for scripts
chown -R root:root $DIR
chmod -R 700 $DIR

# Update
apt-get -q update
apt-get -qqy install locales
echo en_US UTF-8 >> /etc/locale.gen
locale-gen
apt-get -qqy dist-upgrade
apt-get -qqy autoremove
apt-get -q autoclean
apt-get -q clean
apt-get -qqy install ca-certificates

################# Install

# HWE Kernel
apt install --install-recommends linux-generic-hwe-18.04 -qqy
apt install remove linux-generic-18.04 -qqy

# System Configs
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.ipv6.conf.sh
. sysctl.ipv6.conf.sh

# OpenSSH
mkdir -p $DIR/system
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Remote/OpenSSH/openssh.install.sh
. openssh.install.sh

# Iptables
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Networking/IPTables/iptables.install.sh
. iptables.install.sh
#wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/iptables/rules.v4 -O /etc/iptables/rules.v4
#wget -q https://gitlab.com/0DayByte/servers/raw/master/Media/Media-Server/config/iptables/rules.v6 -O /etc/iptables/rules.v6
#iptables-restore < /etc/iptables/rules.v4
#ip6tables-restore < /etc/iptables/rules.v6

# Fail2Ban
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Networking/Fail2Ban/fail2ban.install.sh
. fail2ban.install.sh

# SSL
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/OpenSSL/openssl.install.sh
. openssl.install.sh

# CertBot
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/Lets.Encrypt/certbot.install.deb.sh
. certbot.install.deb.sh

# Nginx
mkdir -p $DIR/webserver
cd $DIR/webserver
wget -q https://gitlab.com/0DayByte/servers/raw/master/Websites/Nginx/nginx.install.sh
. nginx.install.sh

# MariaDB
mkdir -p $DIR/database
cd $DIR/database
wget -q https://gitlab.com/0DayByte/servers/raw/master/Databases/MariaDB/mariadb.install.sh
. mariadb.install.sh

# ViMbAdmin
cd $DIR/system
wget -q https://gitlab.com/0DayByte/servers/-/raw/master/Mail/VimbAdmin/vimbadmin.install.deb.sh
#. vimbadmin.install.deb.sh

echo "Finshed with installing media server on ${hostname} ${distro} ${distro_version}"

#EOF
