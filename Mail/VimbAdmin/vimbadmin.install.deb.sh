#!/usr/bin/env bash

###################################
#
#   ViMbAdmin Install
#   can only user php7.0 or php7.1
#   nothing > php7.1
###################################

vimbadmin_install_path='/var/www/vimbadmin'
vimbadmin_db_password=''

# PHP Repos
apt-get install -y software-properties-common
add-apt-repository -y ppa:ondrej/php

# Requirements
apt-get -q update
apt-get upgrade -qy
apt-get install php7.1-cgi php7.1-mcrypt php-memcache php7.1-json php7.1-mysql php7.1-common php7.1-xml php7.1-fpm\
    postfix postfix-mysql dovecot-core dovecot-imapd dovecot-lmtpd dovecot-mysql dovecot-sieve\
    dovecot-managesieved subversion git zip unzip -qqy

# PHP
sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php/7.1/fpm/php.ini
systemctl restart php7.1-fpm

# Composer
curl -sS https://getcomposer.org/installer -o /usr/local/bin/composer
chmod 755 /usr/local/bin/composer

# - Create User and Group for VimbAdmin
groupadd -g 2000 vmail
useradd -c 'Virtual Mailboxes' -d /var/mail/vhosts -g 2000 -u 2000 -s /usr/sbin/nologin -m vmail

# Database
mysql --execute="CREATE DATABASE vimbadmin;"
mysql --execute="GRANT ALL ON vimbadmin.* TO vimbadmin@localhost IDENTIFIED BY '$vimbadmin_db_password';"
mysql --execute="FLUSH PRIVILEGES;"

# - Install VimbAdmin from Git
git clone https://github.com/opensolutions/ViMbAdmin.git $vimbadmin_install_path
cp $vimbadmin_install_path/application/configs/application.ini.dist $vimbadmin_install_path/application/configs/application.ini
cp $vimbadmin_install_path/public/.htaccess.dist $vimbadmin_install_path/public/.htaccess
cd $vimbadmin_install_path
php /usr/local/bin/composer install
php composer.phar install
. bin/doctrine2-cli.php orm:schema-tool:create

# - All Ownership for Webserver
chown -R www-data:www-data /var/www/vimbadmin

# Cronjob
10 3 * * * root /var/www/vimbadmin/bin/vimbtool.php -a mailbox.cli-get-sizes

#EOF