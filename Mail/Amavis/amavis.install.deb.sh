#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	Amavis
#	Debian/Ubuntu
# Ref https://www.linode.com/docs/email/installing-mail-filtering-for-ubuntu-12-04
###################################################################

# Install Amavis
apt-get -qqy install amavisd-new spamassassin \
    clamav-daemon opendkim postfix-policyd-spf-python \
    pyzor razor arj cabextract cpio lhasa nomarch pax \
    rar unrar unzip zip libnet-dns-perl libmail-spf-perl \
    gzip file zoo clamav clamav-freshclam clamav-daemon \
    libclamunrar7 lrzip p7zip unar unrar-free ripole

# Config/Enable Spamassassin
sed -i -e 's/CRON=0/CRON=1/g' /etc/default/spamassassin
systemctl stop spamassassin
systemctl enable spamassassin
systemctl start spamassassin

# Config/Enable ClamAV
wget -qO /etc/clamav/clamd.conf https://gitlab.com/0DayByte/servers/raw/master/System/Anti.Virus/ClamAV/config/clamd.conf
wget -qO /etc/freshclam.conf https://gitlab.com/0DayByte/servers/raw/master/System/Anti.Virus/ClamAV/config/freshclam.deb.conf
systemctl stop clamav-freshclam
systemctl enable clamav-freshclam
systemctl start clamav-freshclam

# Config/Enable Amavis
wget -qO /etc/amavis/conf.d/15-content_filter_mode https://gitlab.com/0DayByte/servers/raw/master/Mail/Amavis/config/15-content_filter_mode
systemctl stop amavis
systemctl enable amavis
systemctl start amavis

######################################
#
# Add Email for Virus and Spam in
# /etc/amavis/21-ubuntu_defaults
# $virus_admin = 'admin@example.com';
# $spam_admin = 'admin@example.com';
#
######################################

#EOF
