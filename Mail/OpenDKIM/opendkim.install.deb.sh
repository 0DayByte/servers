#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	OpenDKIM
#	Debian/Ubuntu
# ref.
#     https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-dkim-with-postfix-on-debian-wheezy
#
###################################################################

#$domain=example.com

# Install Postfix
apt-get -qqy install opendkim opendkim-tools

# OpenDKIM Configs
mkdir -p /etc/opendkim
wget -qO /etc/opendkim.conf https://gitlab.com/0DayByte/servers/raw/master/Mail/OpenDKIM/configs/opendkim.conf
echo 'SOCKET=inet:8891@localhost' >> /etc/default/opendkim
wget -qO /etc/opendkim/TrustedHosts https://gitlab.com/0DayByte/servers/raw/master/Mail/OpenDKIM/configs/TrustedHosts
echo "*.${domain}" >> /etc/opendkim/TrustedHosts
echo "mail._domainkey.${domain} ${domain}:mail:/etc/opendkim/keys/${domain}/mail.private" > /etc/opendkim/KeyTable
echo "*@${domain} mail._domainkey.${domain}" > /etc/opendkim/SigningTable
mkdir -p /etc/opendkim/keys
mkdir -p /etc/opendkim/keys/${domain}
cd /etc/opendkim/keys/${domain}
opendkim-genkey -b 1024 -s mail -d ${domain}
chown opendkim:opendkim *

# Postfix Congis
echo '# OpenDKIM' >> /etc/postfix/main.cf
echo 'milter_default_action = accept' >> /etc/postfix/main.cf
echo 'milter_protocol = 2' >> /etc/postfix/main.cf
echo 'smtpd_milters = inet:localhost:8891' >> /etc/postfix/main.cf
echo 'non_smtpd_milters = inet:localhost:8891' >> /etc/postfix/main.cf

# Restart Services
systemctl restart postfix opendkim

#EOF
