#!/usr/bin/env bash
set -x #bash verbose

####################################################
#
#	FreePBX
#	Optimized
#
####################################################

# Update
yum -qy update
yum -qy install sangoma-devel 

# System Configs
cd /tmp
wget -qO /tmp/sysctl.conf.sh https://gitlab.com/0DayByte/servers/raw/master/System/System%20Configs/sysctl.conf.sh
. /tmp/sysctl.conf.sh

# Install New Kernel
# Add Repo
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
yum --disablerepo="*" --enablerepo="elrepo-kernel" list available
rpm -e --nodeps kernel-headers-3.10*
# Install Upgraded Kernel
yum --enablerepo=elrepo-kernel -q -y install kernel-ml-devel kernel-ml kernel-ml-headers kernel-ml-tools kernel-ml-tools-libs kernel-ml-tools-libs-devel
yum remove kernel

# Install Mail Hosts Server ################
#rpm -e --nodeps wanpipe-7.0.20-9.sng7.x86_64 kmod-wanpipe-7.0.20-9.sng7.x86_64
#yum install wanpipe-7.0.20.13-1.sng7.x86_64 kmod-wanpipe-7.0.20.13-1.sng7.x86_64

# Install other needed packages:
yum install -q -y git gettext perl-CPAN lrzsz help2man iperf3 easy-rsa

#Uninstall commercial and hardphone related modules & packages
fwconsole ma downloadinstall customcontexts
fwconsole ma downloadinstall extensionsettings
fwconsole ma downloadinstall motif
fwconsole ma downloadinstall pm2
fwconsole ma remove areminder
fwconsole ma remove broadcast
fwconsole ma remove callerid
fwconsole ma remove calllimit
fwconsole ma remove conferencespro
fwconsole ma remove cos
fwconsole ma remove dahdiconfig
fwconsole ma remove digium_phones
fwconsole ma remove restapps
fwconsole ma remove endpoint
#fwconsole ma remove extensionroutes
fwconsole ma remove faxpro
fwconsole ma remove pagingpro
fwconsole ma remove parkpro
fwconsole ma remove pinsetspro
fwconsole ma remove qxact_reports
fwconsole ma remove recording_report
fwconsole ma remove rmsadmin
fwconsole ma remove sangomacrm
fwconsole ma remove sipstation
fwconsole ma remove sms
fwconsole ma remove sng_mcu
fwconsole ma remove vmnotify
fwconsole ma remove voicemail_report
fwconsole ma remove vqplus
fwconsole ma remove webcallback
fwconsole ma remove cxpanel

# Add extended and unsupported repos to search. Yes, weird how need to run three times like this.
fwconsole ma showupgrades
fwconsole ma showupgrades -R extended
fwconsole ma showupgrades -R unsupported
# Upgrade all FreePBX packages
fwconsole ma upgradeall

#
#######################################
# Optimize OS now...
#
# Reduce Apache threads:
cd /etc/httpd/conf
wget https://raw.githubusercontent.com/FreePBXHosting/freepbx-scripts/master/optimizeapache/optimizeapache.sh
bash optimizeapache.sh
# Only needed for digium hardware boards:
service dahdi stop
chkconfig dahdi off
# Stop Auto Login of Root on console (display)
systemctl stop getty@tty1
systemctl disable getty@tty1
# Remove prosody xmpp server from running (already uninstalled from FreePBX Modules):
#service prosody stop
#chkconfig prosody off
# Remove audit:
#rpm -e audit amtu autofs
# Remove support for plug and play devices
#service haldaemon stop
#chkconfig --levels 2345 haldaemon off
#service messagebus stop
#chkconfig messagebus off
# Remove acpid for the physical power button 
#chkconfig acpid off
#chkconfig --levels 2345 acpid off
#rpm -e acpid
# Remove avahi
#rpm -e avahi
# Remove - only needed for Software RAID:
#service stop mdmonitor 
#chkconfig mdmonitor off

# Now please reboot
echo Done Opimitizing your system.
echo PLEASE Reboot your server now
echo type reboot and hit enter
