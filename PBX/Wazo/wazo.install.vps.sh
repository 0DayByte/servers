#!/usr/bin/env bash
set -x #bash verbose

###########################################################
#
# WAZO INSTALL
# http://nerdvittles.com/?page_id=20561
# 
###########################################################

# VPS Install Requirements
apt-get -qqy install cloud-init

# Install
cd /tmp
wget -q http://mirror.wazo.community/fai/xivo-migration/wazo_install.sh
chmod +x wazo_install.sh
bash wazo_install.sh

# # Config
sed -i 's/readonly-idpwd = "true"/readonly-idpwd = "false"/g' /etc/xivo/web-interface/ipbx.ini

#EOF